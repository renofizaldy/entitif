<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Entitif - E-Commerce</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Asap+Condensed:700" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/fotorama.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">

	<link href="css/owl.carousel.min.css" rel="stylesheet">
	<link href="css/owl.theme.default.min.css" rel="stylesheet">

	<script src="js/jquery321.min.js"></script>
	<script src="js/vue244.min.js"></script>

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<header>
		<div class="top-head">
			<!-- <div class="container">
				<i class="fa fa-whatsapp"></i>&nbsp;&nbsp;Layanan Pelanggan 0822-3118-8163
			</div> -->
		</div>
		<div class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-3"><div class="small-logo">
						<img src="img/entitif.png">
					</div></div>
					<div class="col-sm-12 col-md-9">
						<div class="small-menu hidden-xs">
							<ul>
								<li><i class="fa fa-search-plus marginR3" aria-hidden="true"></i>
									<a href="track.php">Lacak Pesanan</a>
								</li>
								<li><i class="fa fa-check-square marginR3" aria-hidden="true"></i>
									<a href="confirm.php">Konfirmasi Pembayaran</a>
								</li>
								<li><i class="fa fa-phone marginR3" aria-hidden="true"></i>
									<a href="contact.php">Kontak</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-default" style="z-index:999">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu-top" aria-expanded="false">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="visible-xs btn-cart navbar-toggle">
						<a href="cart.php"><i class="fa fa-shopping-cart marginR7"></i>
							<span class="badge cart-count" v-cloak>{{cartCount}}</span>
						</a>
					</div>
				</div>

				<div class="collapse navbar-collapse" id="main-menu-top">
					<form class="navbar-form navbar-right visible-xs">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Pencarian ...">
							<span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
						</div>
					</form>
					<ul class="nav navbar-nav nav-menu">
						<li><a href="index.php">Beranda</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produk<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="shop.php">Pria</a></li>
								<li><a href="#">Wanita</a></li>
								<li><a href="#">Something else here</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Halaman<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="shop.php">Info Drop Ship</a></li>
								<li><a href="#">Daftar Promo</a></li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav nav-menu visible-xs">
						<li><a href="track.php">Lacak Pesanan</a></li>
						<li><a href="confirm.php">Konfirmasi Pembayaran</a></li>
						<li><a href="contact.php">Kontak</a></li>
						<!-- <li><a href="pages.php">Tentang</a></li> -->
					</ul>
					<form class="navbar-form navbar-right hidden-xs" method="post">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Pencarian ...">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
						</div>
					</form>
					<ul class="nav navbar-nav navbar-right nav-cart hidden-xs">
						<li><a href="cart.php"><i class="fa fa-shopping-cart marginR7"></i>
							<span class="badge cart-count" v-cloak>{{cartCount}}</span>
						</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>