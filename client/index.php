<?php include("head.php"); ?>

<!-- SLIDER -->
	<div class="container" style="margin-top:15px">
		<div id="front-slide" class="owl-carousel owl-theme" style="width:100%">
			<div class="item">
				<img src='img/slide/converse.jpg'>
			</div>
			<div class="item">
				<img src='img/slide/axs.jpg'>
			</div>
		</div>
	</div>
<!-- END -->

<!-- BODY -->
	<div class="container" id="itemHome">
		<div class="main-wrap">
			<div class="promote-product">
				<div class="row">
					<div class="col-md-7 vcenter">
						<img src="img/slide/yoga520.png">
					</div><!-- 
					 --><div class="col-md-5 text-center vcenter">
						<h3 class="title">A STARTER SET IS THE<br>IDEAL WAY TO START</h3>
						<p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
						<div class="price">
							<span class="pr-before">Rp 11,500,000</span>
							<span class="pr-after">Rp 10,500,000</span>
						</div>
						<div><a href="cart.php" class="btn btn-lg btn-wow"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Beli Sekarang</a></div>
						<div style="margin-top:5px"><small>Berakhir pada 17 Agustus 2017</small></div>
					</div>
				</div>
			</div>
			<div class="space30"></div>
			<h4 class="spb_heading"><span>Produk Terbaru</span></h4>
			<div class="owl-carousel owl-theme">
				<div class="item" v-for="slide in slideImg">
					<div class="res-img"><img :src="`${slide.image}`"></div>
					<div class="product-caption">
						<strong>{{slide.name}}</strong>
						<div>Rp {{slide.price.toLocaleString()}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END -->
<script>
	const slideImg = [
		{
			name: "Baju A",
			price: 35000,
			image: "img/td/1.jpg"
		},
		{
			name: "Baju B",
			price: 35000,
			image: "img/td/2.jpg"
		},
		{
			name: "Baju C",
			price: 35000,
			image: "img/td/3.jpg"
		},
		{
			name: "Baju D",
			price: 35000,
			image: "img/td/4.jpg"
		},
		{
			name: "Baju E",
			price: 35000,
			image: "img/td/5.jpg"
		},
		{
			name: "Baju F",
			price: 35000,
			image: "img/td/6.jpg"
		},
		{
			name: "Baju G",
			price: 35000,
			image: "img/td/8.jpg"
		},
		{
			name: "Baju H",
			price: 35000,
			image: "img/td/12.jpg"
		},
		{
			name: "Baju I",
			price: 35000,
			image: "img/td/11.jpg"
		}
	];
	const home = new Vue({
		el: "#itemHome"
	});
</script>
<?php include('foot.php'); ?>