<?php include("head.php"); ?>

<!-- NAV TITLE -->
	<div class="page-head"><div class="container">
		<div class="heading-text">
			<h1 class="entry-title">Adidas Z.N.E Hoodie</h1>
		</div>
		<div id="breadcrumbs">
			<a title="Go to Kids Category" href="#" class="home">Produk</a> 
			<i class="fa fa-angle-right" aria-hidden="true"></i> 
			<a title="Go to Sport Category" href="#" class="post post-page">Pria</a>
		</div>
	</div></div>
<!-- END -->

<!-- BODY -->
	<div class="container" id="itemDetail">
		<div class="main-wrap">
			<div class="alert alert-info" v-if="showAlert">
				Barang telah di tambahkan ke keranjang.
			</div>
			<div class="row product-detail">
				<div class="col-md-7">
					<div class="fotorama" data-nav="thumbs">
						<img src="img/th/1.jpg">
						<img src="img/th/2.jpg">
						<img src="img/3.jpg">
						<img src="img/th/4.jpg">
						<img src="img/th/5.jpg">
						<img src="img/th/6.jpg">
						<img src="img/th/7.jpg">
						<img src="img/th/8.jpg">
						<img src="img/th/9.jpg">
						<img src="img/th/10.jpg">
						<img src="img/th/11.jpg">
					</div>
				</div>
				<div class="col-md-5 pro-caption">
					<div class="space20 hidden-md hidden-lg"></div>
					<div>
						<label>Harga</label>
						<span class="badge badge-default">15% OFF</span>
					</div>
					<div class="offer">
						<!-- <span class="price">Rp 35,000</span> -->
						<span class="price-before">Rp 2,000,000</span>
						<span class="price-after">Rp 1,499,000</span>
					</div>
					<!-- <div class="offer-detail alert alert-warning">2 Hari Lagi Tawaran Berakhir</div> -->
					<hr>
					<div class="action hidden-xs hidden-sm">
						<a href="#" data-toggle="modal" data-target="#buyConfirm" class="btn btn-lg btn-wow"><i class="fa fa-shopping-cart" aria-hidden="true"></i>beli sekarang</a>
						<a href="#" class="btn btn-lg btn-whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i>beli via whatsapp</a>
						<hr>
					</div>
					<div class="description">
						<p class="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat.</p>
					</div>
					<hr>
					<div class="description">
						<div class="title">Rincian</div>
						<ul class="list-group aditional">
							<li class="list-group-item">Berat <span class="info">0.5 kg</span></li>
							<li class="list-group-item">Ukuran <span class="info">M, L</span></li>
							<li class="list-group-item">Warna <span class="info">Hitam</span></li>
							<li class="list-group-item">Kategori <span class="info">Boys, Sport</span></li>
						</ul>
					</div>
					<hr>
					<div class="description share-product hidden-xs hidden-sm">
						<div class="title">Bagikan</div>
						<i class="fa fa-facebook-square" data-toggle="tooltip" data-placement="top" title="Bagikan di Facebook"></i>
						<i class="fa fa-twitter-square" data-toggle="tooltip" data-placement="top" title="Bagikan di Twitter"></i>
						<i class="fa fa-google-plus-square" data-toggle="tooltip" data-placement="top" title="Bagikan di Google+"></i>
						<i class="fa fa-pinterest-square" data-toggle="tooltip" data-placement="top" title="Bagikan di Pinterest"></i>
					</div>
				</div>
			</div>
			<div class="space50 hidden-xs hidden-sm"></div>
			<div class="product-description">
				<div class="title">Deskripsi</div>
				<p class="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
            <div class="space100"></div>
			<h4 class="spb_heading spb_tabs_heading"><span>PRODUK TERKAIT</span></h4>
			<div class="owl-carousel owl-theme">
				<div class="item" v-for="slide in slideImg">
					<div class="res-img"><img :src="`${slide.image}`"></div>
					<div class="product-caption">
						<strong>{{slide.name}}</strong>
						<div>Rp {{slide.price.toLocaleString()}}</div>
					</div>
				</div>
			</div>
		</div>

		<!-- MODAL -->
			<div class="modal fade modalCartConfirm" id="buyConfirm" tabindex="-1" role="dialog" aria-labelledby="buyConfirm">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-window-close"></i>
							</button>
							<h4 class="modal-title" id="buyConfirm">Beli</h4>
						</div>
						<div class="modal-body">
							<form>
								<div>
									<label>Nama Produk</label>
									<div class="product-name">Adidas Kids Starter Pack</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-xs-4">
										<div><label>Jumlah Barang</label></div>
										<div class="input-group spinner">
											<input name="quantity" type="number" class="form-control product-qty" value="1" v-model="barangQty">
											<div class="input-group-btn-vertical">
												<button class="btn btn-default" type="button" v-on:click="qtyUp">
													<i class="fa fa-chevron-up"></i>
												</button>
												<button class="btn btn-default" type="button" v-on:click="qtyDn">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
									</div>
									<div class="col-xs-8">
										<div><label>Harga Barang</label></div>
										<div class="product-price">{{barangTot}}</div>
									</div>
								</div>
								<hr>
								<div><label>Catatan</label></div>
								<div><textarea class="form-control" name="cart-to-keterangan" placeholder="Contoh: Warna Putih/Ukuran L/Edisi ke-1" v-model="barangInq"></textarea></div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-lg btn-wow" v-on:click="addToCart">
								<i class="fa fa-shopping-cart" aria-hidden="true"></i>tambah ke keranjang
							</button>
							<div class="orSepat"><span>ATAU</span></div>
							<a href="#" class="btn btn-lg btn-whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i>Beli via Whatsapp</a>
						</div>
					</div>
				</div>
			</div>
		<!-- END OF -->
	</div>
<!-- END -->

<!-- FLOAT BUTTON -->
	<div class="navbar navbar-default navbar-fixed-bottom hidden-md hidden-lg" style="box-shadow: 0 -5px 15px -9px #999; border-top:0;border-bottom:0">
		<div class="container text-center" style="padding: 0">
			<div class="row">
				<div class="col-xs-6" style="padding:0"><a data-toggle="modal" data-target="#buyConfirm" style="width:100%;border-radius:0" href="#" class="btn btn-lg btn-wow"><i class="fa fa-shopping-cart"></i>Beli Sekarang</a></div>
				<div class="col-xs-6" style="padding:0"><a style="width:100%;border-radius:0" href="#" class="btn btn-lg btn-whatsapp"><i class="fa fa-whatsapp"></i>Beli via Whatsapp</a></div>
			</div>
		</div>
	</div>
<!-- END OF -->

<script>
	const slideImg = [
		{
			name: "Baju A",
			price: 35000,
			image: "img/td/1.jpg"
		},
		{
			name: "Baju B",
			price: 35000,
			image: "img/td/2.jpg"
		},
		{
			name: "Baju C",
			price: 35000,
			image: "img/td/3.jpg"
		},
		{
			name: "Baju D",
			price: 35000,
			image: "img/td/4.jpg"
		},
		{
			name: "Baju E",
			price: 35000,
			image: "img/td/5.jpg"
		},
		{
			name: "Baju F",
			price: 35000,
			image: "img/td/6.jpg"
		},
		{
			name: "Baju G",
			price: 35000,
			image: "img/td/8.jpg"
		},
		{
			name: "Baju H",
			price: 35000,
			image: "img/td/12.jpg"
		},
		{
			name: "Baju I",
			price: 35000,
			image: "img/td/11.jpg"
		}
	];


	var cartItemExist = JSON.parse(localStorage.getItem('_cart'));
	var itemDetail = {
		id: 3,
		gambar: "http://localhost/entitif/front/client/img/th/1.jpg",
		name: "Adidas Z.N.E Hoodie",
		price: 1475000,
		weight: 500
	};
	const buyConfirm = new Vue({
		el: "#itemDetail",
		data: {
			showAlert: false,
			barangDet: itemDetail,
			barangPrc: itemDetail.price,
			barangInq: "",
			barangQty: 1,
			barangTot: "Rp "+itemDetail.price.toLocaleString()
		},

		methods: {
			qtyUp() {
				var equ = Math.abs(parseInt(this.barangQty, 10) +1);
				this.barangQty = equ;
				this.barangTot = "Rp "+(this.barangPrc*equ).toLocaleString();
			},
			qtyDn() {
				var equ = Math.abs(parseInt(this.barangQty, 10) -1);
				this.barangQty = equ;
				this.barangTot = "Rp "+(this.barangPrc*equ).toLocaleString();
			},
			addToCart() {
				var statusExist = 0;
				var itemId;
				for (i=0; i<cartItemExist.length; i++) {
					if (cartItemExist[i].id == itemDetail.id) {
						statusExist = 1;
						itemId = i;
					}
				}
				if (statusExist == 1) {
					// READ & APPEND
					var itemAppend = {
						id    	: itemDetail.id,
						gambar	: itemDetail.gambar,
						name  	: itemDetail.name,
						price 	: itemDetail.price,
						weight	: itemDetail.weight,
						quantity: (cartItemExist[itemId].quantity+this.barangQty),
						inquiry : cartItemExist[itemId].inquiry+" "+this.barangInq
					}
					// DELETE
					delete cartItemExist[itemId];
					cartItemExist = cartItemExist.filter(function(x) { return x !== null });
					// WRITE
					cartItemExist.push(itemAppend);
					localStorage.removeItem('_cart');
					localStorage.setItem('_cart', JSON.stringify(cartItemExist));
					$("#buyConfirm").modal('hide');
					this.showAlert = true;
				} else {
					// READ & APPEND
					var itemAppend = {
						id    	: itemDetail.id,
						gambar	: itemDetail.gambar,
						name  	: itemDetail.name,
						price 	: itemDetail.price,
						weight	: itemDetail.weight,
						quantity: this.barangQty,
						inquiry : this.barangInq
					}
					// WRITE
					cartItemExist.push(itemAppend);
					localStorage.removeItem('_cart');
					localStorage.setItem('_cart', JSON.stringify(cartItemExist));
					$("#buyConfirm").modal('hide');
					this.showAlert = true;
				}
			}
		},
		watch: {
			'barangQty': function(val) {
				if (val < 0 || val == 0) {
					this.barangQty = 1;
				} else {
					this.barangTot = "Rp "+(this.barangPrc*val).toLocaleString();
				}
			}
		}
	});
</script>

<?php include('foot.php'); ?>