<?php include("head.php"); ?>

<!-- SLIDER -->
	<div class="page-head"><div class="container">
		<div class="heading-text">
			<h1 class="entry-title">Shop</h1>
		</div>
		<div id="breadcrumbs">
			<a title="Go to Neighborhood." href="#" class="home">Produk</a> 
			<i class="fa fa-angle-right" aria-hidden="true"></i> 
			<a title="Go to Pages." href="#" class="post post-page">Pria</a>
		</div>
	</div></div>
<!-- END -->
<style type="text/css">
	.head-result-count {
		font-size: 14px;
	}
</style>
<!-- BODY -->
	<div class="container">
		<div class="main-wrap">
			<div class="row">
				<div class="col-md-3">
					<div class="head-result-count">Menampilkan 1-10 dari 20 Produk</div>
				</div>
				<div class="col-md-7"></div>
				<div class="col-md-2">
					<div class="form-group" style="margin-bottom: 0">
						<select class="form-control">
							<option>Urutkan</option>
							<option>Terpopuler</option>
							<option>Terbaru</option>
							<option>Termurah</option>
							<option>Termahal</option>
						</select>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="detail.php">
						<div class="pro-img">
							<div class="pro-disc">-25%</div>
							<img src='img/th/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Z.N.E Hoodie</h2>
							<div class="price">
								<span class="before">Rp 2,000,000</span>
								<span class="after">Rp 1,499,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/th/2.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Marvel Avengers Boy</h2>
							<div class="price">
								<span>Rp 50,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<div class="pro-disc">-50%</div>
							<img src='img/td/3.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Gildan for Kids</h2>
							<div class="price">
								<span class="before">Rp 250,000</span>
								<span class="after">Rp 125,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<div class="pro-disc">-35%</div>
							<img src='img/td/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title" alt="Adidas Kids Starter Pack">Adidas Kids Starter Pack</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/td/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Baju Bekas Anak</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/td/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Baju Bekas Anak</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
			</div>
		</div>
	</div>
<!-- END -->

<?php include('foot.php'); ?>