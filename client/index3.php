<?php include("head.php"); ?>

<!-- SLIDER -->
	<div class="container" style="margin-top:15px">
		<div id="front-slide" class="owl-carousel owl-theme" style="width:100%">
			<div class="item">
				<img src='img/converse.jpg'>
			</div>
			<div class="item">
				<img src='img/axs.jpg'>
			</div>
		</div>
	</div>
<!-- END -->

<!-- BODY -->
	<div class="container">
		<div class="main-wrap">
			<div class="promote-product">
				<div class="row">
					<div class="col-md-7 vcenter">
						<img src="img/starter-sets-3-piece-desktop-d161c1c2.png" style="width: 100%">
					</div><!-- 
					 --><div class="col-md-5 text-center vcenter">
						<h3>A STARTER SET IS THE<br>IDEAL WAY TO START</h3>
						<p style="font-size: 16px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
						<div class="price">
							<span class="pr-before">Rp 500,000</span>
							<span class="pr-after">Rp 249,999</span>
						</div>
						<div><a href="cart.php" class="btn btn-lg btn-wow"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Beli Sekarang</a></div>
						<!-- <div><a href="#" class="btn btn-lg btn-whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i>Beli via Whatsapp</a></div> -->
						<div style="margin-top:5px"><small>Berakhir pada 17 Agustus 2017</small></div>
						<!-- <div style='margin-top:5px'><small>Beli via Whatsapp? Klik disini</small></div> -->
					</div>
				</div>
			</div>
            <div class="space30"></div>

            <!-- <div class="space30">&nbsp;</div> <div class="space30">&nbsp;</div> -->
			<h4 class="spb_heading spb_tabs_heading"><span>For Your Sport</span></h4>
			<div class="row">
				<div class="col-md-4">
					<img class="img-responsive" src="https://static-id.zacdn.com/cms/CAMPAIGNS/SIS-Adidas_June_06_women.jpg">
					<div class="featured-promo">
						<h5>Female Originals</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt</p>
					</div>
				</div>
				<div class="col-md-4">
					<img class="img-responsive" src="https://static-id.zacdn.com/cms/CAMPAIGNS/SIS-Adidas_June_08_women.jpg">
					<div class="featured-promo">
						<h5>Female Performance</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt</p>
					</div>
				</div>
				<div class="col-md-4">
					<img class="img-responsive" src="https://static-id.zacdn.com/cms/CAMPAIGNS/SIS-Adidas_June_men_06.jpg">
					<div class="featured-promo">
						<h5>Male Originals</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt</p>
					</div>
				</div>
			</div>
			<h4 class="spb_heading"><span>FREQUENTLY ASKED QUESTIONS</span></h4>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div id="accordion" class="checkout">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
							<div class="panel checkout-step">
								<div class="row">
									<div class="col-xs-10">
										<h4 class="checkout-step-title">Bagaimana Cara Kerja Kami ?</h4>
									</div>
									<div class="col-xs-2 text-right">
										<i class="fa fa-angle-down"></i>
									</div>
								</div>
								<div id="collapseOne" class="collapse in">
									<div class="checkout-step-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
										proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
							</div>
						</a>
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
							<div class="panel checkout-step">
								<div role="tab" id="headingTwo" class="row">
									<div class="col-xs-10">
										<h4 class="checkout-step-title">Kapan waktu pengiriman order ?</h4>
									</div>
									<div class="col-xs-2 text-right">
										<i class="fa fa-angle-down"></i>
									</div>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse">
									<div class="checkout-step-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
										proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
							</div>
						</a>
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
							<div class="panel checkout-step">
								<div role="tab" id="headingThree" class="row">
									<div class="col-xs-10">
										<h4 class="checkout-step-title">Apakah barang dijamin original ?</h4>
									</div>
									<div class="col-xs-2 text-right">
										<i class="fa fa-angle-down"></i>
									</div>
								</div>
								<div id="collapseThree" class="panel-collapse collapse">
									<div class="checkout-step-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
										proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
							</div>
						</a>
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
							<div class="panel checkout-step">
								<div role="tab" id="headingFour" class="row">
									<div class="col-xs-10">
										<h4 class="checkout-step-title">Bagaiman melakukan pembayaran dengan Kartu Kredit ?</h4>
									</div>
									<div class="col-xs-2 text-right">
										<i class="fa fa-angle-down"></i>
									</div>
								</div>
								<div id="collapseFour" class="panel-collapse collapse">
									<div class="checkout-step-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
										proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END -->

<?php include('foot.php'); ?>