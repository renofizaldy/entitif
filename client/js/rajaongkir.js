const rajaOngkir    = "https://api.rajaongkir.com/starter/";
const rajaOngkirSub = "?province=11";
$.ajax({
	url 		: rajaOngkir + 'city' + rajaOngkirSub,
	headers 	: {"key": "4f0086aaac524a3b5012761520e7bc4b"},
	type 		: "GET",
	dataType	: "JSON",
	crossDomain : true,
	cache 		: true,
	success		: function(data) {
		console.log(data);
	}
});

// var localCache = {
//     data: {},
//     remove: function (url) {
//         delete localCache.data[url];
//     },
//     exist: function (url) {
//         return localCache.data.hasOwnProperty(url) && localCache.data[url] !== null;
//     },
//     get: function (url) {
//         console.log('Getting in cache for url' + url);
//         return localCache.data[url];
//     },
//     set: function (url, cachedData, callback) {
//         localCache.remove(url);
//         localCache.data[url] = cachedData;
//         if ($.isFunction(callback)) callback(cachedData);
//     }
// };

// $(function () {
//     var url = '/echo/jsonp/';
//     $('#ajaxButton').click(function (e) {
//         $.ajax({
//             url: url,
//             data: {
//                 test: 'value'
//             },
//             cache: true,
//             beforeSend: function () {
//                 if (localCache.exist(url)) {
//                     doSomething(localCache.get(url));
//                     return false;
//                 }
//                 return true;
//             },
//             complete: function (jqXHR, textStatus) {
//                 localCache.set(url, jqXHR, doSomething);
//             }
//         });
//     });
// });

// function doSomething(data) {
//     console.log(data);
// }