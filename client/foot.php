<!-- FOOTER -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h4 class="spb_heading"><span>Tentang</span></h4>
					<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat, laboris nisi ut aliquip ex ea commodo
					consequat exercitation ullamco laboris incididunt ut labore et dolore magna aliqua.</p>
					<div class="space30 hidden-md hidden-lg"></div>
				</div>
				<div class="col-md-3">
					<h4 class="spb_heading"><span>Halaman</span></h4>
					<ul class="nav-foot">
						<a href="track.php"><li>Lacak Pesanan<span class="link"><i class="fa fa-angle-right"></i></span></li></a>
						<a href="confirm.php"><li>Konfirmasi Pembayaran<span class="link"><i class="fa fa-angle-right"></i></span></li></a>
						<a href="pages.php"><li>Tentang<span class="link"><i class="fa fa-angle-right"></i></span></li></a>
						<a href="#"><li>Kontak<span class="link"><i class="fa fa-angle-right"></i></span></li></a>
					</ul>
					<div class="space30 hidden-md hidden-lg"></div>
				</div>
				<div class="col-md-5">
					<h4 class="spb_heading"><span>Saluran dan Dukungan</span></h4>
					<div class="external-logo">
						<label class="external-logo-title">Saluran Kami</label>
						<ul class="external-logo-lists">
							<li class="external-logo--list">
								<img src="img/ico-sos/facebook.png">
								<img src="img/ico-sos/instagram.png">
								<img src="img/ico-sos/twitter.png">
								<img src="img/ico-sos/youtube.png">
								<img src="img/ico-sos/whatsapp.png">
								<img src="img/ico-sos/bbm.png">
								<img src="img/ico-sos/line.png">
							</li>
						</ul>
					</div>
					<div class="external-logo">
						<label class="external-logo-title">Bank Transfer</label>
						<ul class="external-logo-lists">
							<li class="external-logo--list">
								<img src="img/bank/b_bca.jpg" />
								<img src="img/bank/b_bni.jpg" />
								<img src="img/bank/b_bri.jpg" />
								<img src="img/bank/b_mandiri.jpg" />
							</li>
						</ul>
					</div>
					<div class="external-logo">
						<label class="external-logo-title">Jasa Pengiriman</label>
						<ul class="external-logo-lists">
							<li class="external-logo--list">
								<img src="img/kurir/b_jne.jpg" />
								<img src="img/kurir/b_tiki.jpg" />
								<img src="img/kurir/b_pos.jpg" />
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="copyright">
		<div class="container">Hak Cipta &copy; <?php echo date('Y'); ?>. Entitif — By <a href="http://entitif.com" target="_blank">Entitif Commerce</a></div>
	</div>
<!-- END -->

	<script src="js/bootstrap.min.js"></script>
	<script src="js/fotorama.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>

	<script>
		const head = new Vue({
			el: "header",
			data: {
				count: 0,
				getCart: JSON.parse(localStorage.getItem('_cart'))
			},
			computed: {
				cartCount: function() {
					if (this.getCart.length > 0) {
						for (i=0;i<this.getCart.length;i++) {
							this.count += +this.getCart[i].quantity;
						}
						return this.count;
					} else {
						return 0;
					}
				}
			}
		});
	</script>
	<!-- <script src="js/rajaongkir.js"></script> -->
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
			$('#front-slide').owlCarousel({
				loop: false,
				autoplay: true,
				lazyLoad: true,
				autoplayTimeout:3000,
				items: 1
			});
			$('.owl-carousel').owlCarousel({
				autoplay:true,
				autoplayTimeout:3000,
				loop:true,
				lazyLoad: true,
				margin:10,
				navText: ["<i class='fa fa-arrow-left'></i>", "<i class='fa fa-arrow-right'></i>"], 
				navElement: 'div',
				navContainer: false,
				navContainerClass: 'owl-nav',
				navClass: ['owl-prev', 'owl-next'],
				slideBy: 1,
				dots: false,
				responsiveClass: true,
				responsive:{
					0:{
						items:2,
						nav:true
					},
					600:{
						items:3,
						nav:true
					},
					1000:{
						// stagePadding: 50,
						items:5,
						nav:true,
						loop:true
					}
				}
			});
		});
	</script>
</body>
</html>