<?php include("head.php"); ?>

<!-- NAV TITLE -->
	<div class="page-head"><div class="container">
		<div class="heading-text">
			<h1 class="entry-title">Keranjang Belanja</h1>
		</div>
	</div></div>
<!-- END -->

<!-- BODY -->
	<div class="container" id="cartPayment">
		<div class="main-wrap">
			<div class="row product-detail">
				<div class="col-md-6">
					<div class="hidden-md hidden-lg well-cart-sm" v-cloak>
						<label data-toggle="collapse" data-target="#cartTable">
							<i class="fa fa-chevron-down"></i>&nbsp;&nbsp;LIHAT DAFTAR PEMBELIAN
						</label>
						<div id="cartTable" class="collapse in well">
							<table class="cart-checkout">
								<tbody>
									<tr v-for="barang in items">
										<td class="w-img"><img :src="`${barang.gambar}`"></td>
										<td class="w-det">
											<div class="w-tit"><strong>{{barang.name}}</strong></div>
											<div><strong>{{barang.quantity}} Barang ({{barang.weight}} kg)</strong> x Rp {{barang.price.toLocaleString()}}</div>
											<div>{{barang.inquiry}}</div>
											<div class="label label-default">
												<span class="action" v-on:click="selectInCart(barang)"><i class="fa fa-edit"></i> Ubah</span> - 
												<span class="action" v-on:click="deleteInCart(barang)"><i class="fa fa-trash"></i> Hapus</span>
											</div>
										</td>
										<td class="w-prc">
											Rp {{(barang.quantity * barang.price).toLocaleString()}}
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="hidden-xs hidden-sm well-cart-lg" v-cloak>
						<label>DETAIL PEMBELIAN</label>
						<hr class="separator15">
						<div class="well">
							<table class="cart-checkout">
								<tbody>
									<tr v-for="barang in items">
										<td class="w-img"><img :src="`${barang.gambar}`"></td>
										<td class="w-det">
											<div class="w-tit"><strong>{{barang.name}}</strong></div>
											<div><strong>{{barang.quantity}} Barang ({{barang.weight}} kg)</strong> x Rp {{barang.price.toLocaleString()}}</div>
											<div>{{barang.inquiry}}</div>
											<div class="label label-default">
												<span class="action" v-on:click="selectInCart(barang)"><i class="fa fa-edit"></i> Ubah</span> - 
												<span class="action" v-on:click="deleteInCart(barang)"><i class="fa fa-trash"></i> Hapus</span>
											</div>
										</td>
										<td class="w-prc">
											Rp {{(barang.quantity * barang.price).toLocaleString()}}
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div v-show="showDiskonParent">
							<div v-if="showDiskonChild">
								<label>{{judul}} <span href="#" v-on:click="showDiskonChild = !showDiskonChild">Batalkan</span></label>
								<div class="input-group">
									<input type="text" class="form-control" v-model="userKupon">
									<span class="input-group-btn">
										<button class="btn btn-main btn-nowidth" v-on:click="cekDiskon" type="button">{{tombol}}</button>
									</span>
								</div>
							</div>
							<label v-else>{{couponAdd}} <span href="#" v-on:click="showDiskonChild = !showDiskonChild">Klik disini</span></label>
						</div>
						<hr>
						<table class="tb-pricing">
							<tbody>
								<tr>
									<td class="st-title">Subtotal</td>
									<td class="st-price">Rp {{cartSubTotal.toLocaleString()}}</td>
								</tr>
								<tr>
									<td class="bk-title">Biaya Kirim</td>
									<td class="bk-price">Rp {{cartBiayaKirim.toLocaleString()}}</td>
								</tr>
								<tr v-if="diskonInfo">
									<td class="bk-title">Voucher: "<b>{{diskonName}}</b>"</td>
									<td class="bk-price">Rp -{{diskonPrice.toLocaleString()}}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td class="tp-title">Total Pembayaran</td>
									<td class="tp-price">Rp {{cartTotal.toLocaleString()}}</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<div class="col-md-6">
					<label>DETAIL PENGIRIMAN</label>
					<hr class="separator15">
					<form action="" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label">Nama Lengkap</label>:
							<input type="text" class="form-control" placeholder="Tulis nama lengkap">
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<label class="control-label">Email</label>
								<input type="text" class="form-control" placeholder="Tulis email anda">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Telepon/Ponsel</label>
								<input type="text" class="form-control" placeholder="Nomor Telpon/Ponsel">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">Alamat Lengkap</label>
							<textarea class="form-control" placeholder="Alamat Lengkap, Kelurahan, Kecamatan, dan Kode Pos"></textarea>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label>Provinsi</label>
								<select class="form-control">
									<option>Pilih</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Kota</label>
								<select class="form-control">
									<option>Pilih</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Kurir</label>
								<select class="form-control">
									<option>Pilih</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Catatan (Opsional)</label>
							<textarea class="form-control"></textarea>
						</div>
						<hr>
						<div class="form-group">
							<label>Metode Pembayaran</label>: 
							<span class="label label-default">Bank Transfer</span>
						</div>
						<hr>
						<div class="hidden-md hidden-lg">
							<div v-show="showDiskonParent">
								<div v-if="showDiskonChild" id="showDiskonChild">
									<label>{{judul}} <span href="#" v-on:click="showDiskonChild = !showDiskonChild">Batalkan</span></label>
									<div class="input-group">
										<input class="form-control" placeholder="">
										<span class="input-group-btn">
											<button class="btn btn-main btn-nowidth" v-on:click="cekDiskon" type="button">{{tombol}}</button>
										</span>
									</div>
								</div>
								<label v-else>{{couponAdd}} <span v-on:click="showDiskonChild = !showDiskonChild">Klik disini</span></label>
							</div>
							<hr>
							<table class="tb-pricing">
								<tbody>
									<tr>
										<td class="st-title">Subtotal</td>
										<td class="st-price">{{cartSubTotal}}</td>
									</tr>
									<tr>
										<td class="bk-title">Biaya Kirim</td>
										<td class="bk-price">{{cartBiayaKirim}}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td class="tp-title">Total Pembayaran</td>
										<td class="tp-price">{{cartTotal}}</td>
									</tr>
								</tfoot>
							</table>
							<hr>
						</div>
						<a href="#" class="btn btn-wow btn-main btn-lg">
							SELESAI BELANJA<i class="fa fa-check right" aria-hidden="true"></i>
						</a>
					</form>
				</div>
			</div>
		</div>

		<!-- MODAL -->
		<div class="modal fade modalCartConfirm" id="cartEdit" tabindex="-1" role="dialog" aria-labelledby="cartEdit">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i class="fa fa-window-close"></i>
						</button>
						<h4 class="modal-title" id="cartEdit">Ubah</h4>
					</div>
					<div class="modal-body">
						<form>
							<div>
								<label>Nama Produk</label>
								<div class="product-name">{{cartModal.name}}</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-xs-4">
									<div><label>Jumlah Barang</label></div>
									<div class="input-group spinner">
										<input name="barangQty" type="number" class="form-control product-qty" value="1" v-model="barangQty">
										<div class="input-group-btn-vertical">
											<button class="btn btn-default" type="button" v-on:click="qtyUp">
												<i class="fa fa-chevron-up"></i>
											</button>
											<button class="btn btn-default" type="button" v-on:click="qtyDn">
												<i class="fa fa-chevron-down"></i>
											</button>
										</div>
									</div>
								</div>
								<div class="col-xs-8">
									<div><label>Harga Barang</label></div>
									<div class="product-price">{{barangTot}}</div>
								</div>
							</div>
							<hr>
							<div><label>Catatan</label></div>
							<div><textarea class="form-control" name="cart-to-keterangan" placeholder="Contoh: Warna Putih/Ukuran L/Edisi ke-1" v-model="cartModal.inquiry"></textarea></div>
						</form>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-lg btn-main" v-on:click="updateInCart(cartModal.id)">Simpan perubahan</a>
					</div>
				</div>
			</div>
		</div>
		<!-- END OF -->
	</div>
<!-- END -->

<script>
	// const cartItemX = [
	// 	{
	// 		id: 1,
	// 		gambar: "http://localhost/entitif/front/client/img/td/1.jpg",
	// 		name: "Adidas Kids Starter Pack",
	// 		quantity: 2,
	// 		price: 250000,
	// 		weight: 500,
	// 		inquiry: "Ukuran S Tolong dikirim secepatnya"
	// 	},
	// 	{
	// 		id: 2,
	// 		gambar: "http://localhost/entitif/front/client/img/td/2.jpg",
	// 		name: "Kids Marvel",
	// 		quantity: 1,
	// 		price: 270000,
	// 		weight: 500,
	// 		inquiry: "Warna Grey ya"
	// 	}
	// ];
	// localStorage.setItem('_cart', JSON.stringify(cartItemX));

	const voucher  	= JSON.parse(localStorage.getItem('_voucher'));
	var cartItem 	= JSON.parse(localStorage.getItem('_cart'));
	const cartPayment = new Vue({
		el: "#cartPayment",
		data: {
			items    		: cartItem,
			diskon 			: [{
					kupon: "TEST123",
					price: 40000,
					type : "ph"
				}, {
					kupon: "123TEST",
					price: 0.3,
					type : "pp",
				}, {
					kupon: "ONGKIR",
					price: 0,
					type : "of"
				}
			],

			showDiskonChild : false,

			judul     		: "Masukkan kode voucher.",
			couponAdd 		: "Punya kode voucher?",
			tombol    		: "Gunakan",
			cartBiayaKirim	: 25000,

			barangQty 		: "",
			barangTot 		: "",
			cartModal		: "",
			userKupon 		: ""
		},
		methods: {
			cekDiskon() {
				var cekDiskonCondition = false;
				for (i=0; i < this.diskon.length; i++) {
					if (this.userKupon.toUpperCase() == this.diskon[i].kupon) {
						var userActiveKupon = this.diskon[i].kupon;
						var userActivePrice = this.diskon[i].price;
						var userActiveType  = this.diskon[i].type;
						cekDiskonCondition = true;
					}
				}
				if (cekDiskonCondition) {
					var voucherItemX = [{
						kode : userActiveKupon,
						price: userActivePrice,
						type : userActiveType
					}];
					localStorage.setItem('_voucher', JSON.stringify(voucherItemX));
					location.reload();
				}
			},
			qtyUp() {
				var equ = Math.abs(parseInt(this.barangQty, 10) +1);
				this.barangQty = equ;
				this.barangTot = "Rp "+(this.cartModal.price*equ).toLocaleString();
			},
			qtyDn() {
				var equ = Math.abs(parseInt(this.barangQty, 10) -1);
				this.barangQty = equ;
				this.barangTot = "Rp "+(this.cartModal.price*equ).toLocaleString();
			},
			selectInCart(val) {
				for (i=0;i<cartItem.length; i++) {
					if (cartItem[i].id == val.id) {
						this.cartModal = {
							id       : cartItem[i].id,
							gambar   : cartItem[i].gambar,
							name     : cartItem[i].name,
							quantity : cartItem[i].quantity,
							price    : cartItem[i].price,
							weight   : cartItem[i].weight,
							inquiry  : cartItem[i].inquiry
						};
						this.barangQty = cartItem[i].quantity;
						this.barangTot = "Rp "+(cartItem[i].price*cartItem[i].quantity).toLocaleString();
						$("#cartEdit").modal();
					}
				}
			},
			updateInCart(val) {
				var itemNew = {
					id       : this.cartModal.id,
					gambar   : this.cartModal.gambar,
					name     : this.cartModal.name,
					quantity : this.barangQty,
					price    : this.cartModal.price,
					weight   : this.cartModal.weight,
					inquiry  : this.cartModal.inquiry
				};
	
				for (i=0; i < cartItem.length; i++) {
					if (cartItem[i].id === val) {
						delete cartItem[i];
						cartItem = cartItem.filter(function(x) { return x !== null });
						cartItem.push(itemNew);
						localStorage.removeItem('_cart');
						localStorage.setItem('_cart', JSON.stringify(cartItem));
						location.reload();
						// this.$forceUpdate();
					}
				}
			},
			deleteInCart(val) {
				if (confirm('Hapus dari keranjang?')) {
					for (i=0; i < cartItem.length; i++) {
						if (cartItem[i].id === val.id) {
							delete cartItem[i];
							cartItem = cartItem.filter(function(x) { return x !== null });
							localStorage.removeItem('_cart');
							localStorage.setItem('_cart', JSON.stringify(cartItem));
							location.reload();
						}
					}
				} else {
					return false;
				}
			}
		},
		computed: {
			cartSubTotal  	: function() {
				var total = [];
				Object.entries(cartItem).forEach(([key, val]) => {
					total.push(val.price*val.quantity)
				});
				return total.reduce(function(total, num) { return total + num }, 0);
			},
			cartTotal 		: function() {
				var defaultPrice = this.cartSubTotal + this.cartBiayaKirim;
				if (!voucher) {
					return defaultPrice;
				}
				switch(this.diskonType) {
					case 'pp':
						return (this.cartSubTotal + this.cartBiayaKirim) - this.diskonPrice;
					break;
					case 'ph':
						if (this.diskonPrice !== 0 || this.diskonPrice > 0) {
							return (this.cartSubTotal + this.cartBiayaKirim) - this.diskonPrice;
						}
					break;
					case 'of':
						if (this.diskonPrice !== 0 || this.diskonPrice > 0) {
							return (this.cartSubTotal + this.cartBiayaKirim) - this.cartBiayaKirim;
						}
					break;
					default:
						return defaultPrice;
					break;
				}
			},
			showDiskonParent: function() {
				if (!voucher) {
					return true;
				} return false;
			},
			diskonInfo		: function() {
				if (!voucher) {
					return false;
				} return true;
			},
			diskonName 		: function() {
				if (!voucher) {
					return "";
				}
				var res = "";
				Object.entries(voucher).forEach(([key, val]) => {
					res = val.kode;
				});
				return res.toUpperCase();
			},
			diskonPrice 	: function() {
				if (!voucher) {
					return 0;
				}
				var res = "";
				var typ = "";
				Object.entries(voucher).forEach(([key, val]) => {
					typ = val.type;
					res = val.price;
				});
				switch(typ) {
					case 'pp':
						return res * (this.cartSubTotal + this.cartBiayaKirim);
					break;
					case 'of':
						return this.cartBiayaKirim;
					break;
					default:
						return res;
					break;
				}
			},
			diskonType 		: function() {
				if (!voucher) {
					return "";
				}
				var res = "";
				Object.entries(voucher).forEach(([key, val]) => {
					res = val.type;
				});
				return res;
			}
		},
		watch: {
			'barangQty': function(val) {
				if (val < 0 || val == 0) {
					this.barangQty = 1;
				} else {
					this.barangTot = "Rp "+(this.cartModal.price*val).toLocaleString();
				}
			}
		}
	});
</script>

<?php include('foot.php'); ?>