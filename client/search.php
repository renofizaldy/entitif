<?php include("head.php"); ?>

<!-- SLIDER -->
	<div class="page-head"><div class="container">
		<div class="heading-text">
			<h1 class="entry-title">Pencarian</h1>
		</div>
		<div id="breadcrumbs">
			<a title="Go to Neighborhood." href="#" class="home">Men</a> 
			<i class="fa fa-angle-right" aria-hidden="true"></i> 
			<a title="Go to Pages." href="#" class="post post-page">Sport</a>
		</div>
	</div></div>
<!-- END -->
<style type="text/css">
	.head-result-count {
		font-size: 14px;
	}
</style>
<!-- BODY -->
	<div class="container">
		<div class="main-wrap">
			<div class="head-result-count">Ditemukan 3 Produk untuk pencarian "Baju anak"</div>
			<hr>
			<div class="row">
				<div class="col-md-3">
	                <div class="product-list"><a href="detail.php">
	                    <div class="pro-img">
	                    	<div class="pro-disc">-35%</div>
	                    	<img src='img/1.jpg'>
	                    </div>
	                    <div class="pro-caption">
	                    	<div class="title">Adidas Kids Starter Pack</div>
	                    	<div class="price">
	                    		<span class="before">Rp 35,000</span>
	                    		<span class="after">Rp 25,000</span>
	                    	</div>
	                    </div>
	                </a></div>
				</div>
				<div class="col-md-3">
	                <div class="product-list"><a href="#">
	                    <div class="pro-img">
	                    	<img src='img/2.jpg'>
	                    </div>
	                    <div class="pro-caption">
	                    	<div class="title">Marvel Avengers Boy</div>
	                    	<div class="price">
	                    		<span>Rp 50,000</span>
	                    	</div>
	                    </div>
	                </a></div>
				</div>
				<div class="col-md-3">
	                <div class="product-list"><a href="#">
	                    <div class="pro-img">
	                    	<div class="pro-disc">-50%</div>
	                    	<img src='img/3.jpg'>
	                    </div>
	                    <div class="pro-caption">
	                    	<div class="title">Gildan for Kids</div>
	                    	<div class="price">
	                    		<span class="before">Rp 250,000</span>
	                    		<span class="after">Rp 125,000</span>
	                    	</div>
	                    </div>
	                </a></div>
				</div>
			</div>
		</div>
	</div>
<!-- END -->

<?php include('foot.php'); ?>