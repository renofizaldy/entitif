<?php include("head.php"); ?>

<!-- NAV TITLE -->
	<div class="page-head"><div class="container">
		<div class="heading-text">
			<h1 class="entry-title">Kontak Kami</h1>
		</div>
	</div></div>
<!-- END -->

<style type="text/css">
	#map {
		width: 100%;
		height: 300px;
	}
</style>

<!-- BODY -->
	<div class="container">
		<div class="main-wrap">
			<div class="row product-detail">
				<div class="col-md-8">
					<p class="font14">Silahkan tulis pesan di bawah ini untuk pertanyaan seputar produk dan layanan kami.</p>
					<hr>
					<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label">Tulis Pesan</label>
							<div class="col-sm-9">
								<textarea class="form-control" style="min-height: 75px"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Nama Lengkap</label>
							<div class="col-sm-9">
								<input type="text" id="nama" name="nama" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<input type="email" id="email" name="email" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Telepon (Opsional)</label>
							<div class="col-sm-9">
								<input type="email" id="email" name="email" class="form-control">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<div><img src="img/captcha.jpg"></div>
								<div class="space10"></div>
								<div style="width: 50%">
									<input class="form-control" required="" type="text" name="captcha" placeholder="Tulis angka verifikasi...">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button class="btn btn-main btn-lg">KIRIM PESAN<i class="fa fa-envelope right"></i></button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4">
					<hr class="hidden-md hidden-lg">
					<div class="well">
						<label><i class="fa fa-map-signs"></i> Alamat</label>
						<div class="font16">Jl. Soekarno-Hatta, No.1<br>Malang - 65112<br>East Java - Indonesia</div>
						<div class="space10"></div>
						<label><i class="fa fa-phone"></i> Telepon</label>
						<div class="font16">0822 3118 8163</div>
						<div class="space10"></div>
						<label><i class="fa fa-envelope"></i> Email</label>
						<div class="font16">info@entitif.com</div>
					</div>
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>
<!-- END -->

<script>
	function initMap() {
		var uluru = {lat: -25.363, lng: 131.044};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 13,
			center: uluru
		});
		var marker = new google.maps.Marker({
			position: uluru,
			map: map
		});
	}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCe3LVBaPcG2BVVfL2JGr9WcI2LPz7VOQ&callback=initMap">
</script>

<?php include('foot.php'); ?>