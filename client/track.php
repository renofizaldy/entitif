<?php include("head.php"); ?>

<!-- NAV TITLE -->
	<div class="page-head"><div class="container">
		<div class="heading-text">
			<h1 class="entry-title">Lacak Pesanan</h1>
		</div>
	</div></div>
<!-- END -->

<!-- BODY -->
	<div class="container">
		<div class="main-wrap">
			<div class="row product-detail">
				<div class="col-md-12 text-center">
					<p class="font16">Silahkan masukkan Nomor Faktur anda</p>
					<div class="form-input-track">
						<hr>
						<div><input class="form-control" name="nomor_faktur_track" placeholder="Nomor Faktur" type="text"></div>
						<br>
						<div class="form-group">
							<button class="btn btn-main btn-lg">Lacak Pesanan</button>
						</div>
					</div>
				</div>
			</div>
			<br><br><br>
			<div class="well">
				<p class="font16">Hasil Pelacakan Nomor Faktur: <strong>274668292</strong></p>
				<hr>
				<h4>Tidak Ditemukan!</h4>
			</div>
		</div>
	</div>
<!-- END -->

<?php include('foot.php'); ?>