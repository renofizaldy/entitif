<?php include("head.php"); ?>

<!-- NAV TITLE -->
	<div class="page-head"><div class="container">
		<div class="heading-text">
			<h1 class="entry-title">Konfirmasi Pembayaran</h1>
		</div>
		<div id="breadcrumbs">
			<a title="Go to Kids Category" href="#" class="home">Halaman</a> 
			<i class="fa fa-angle-right" aria-hidden="true"></i> 
			<a title="Go to Sport Category" href="#" class="post post-page">Konfirmasi</a>
		</div>
	</div></div>
<!-- END -->

<style type="text/css">
	.well {
		border-radius: 2px;
	}
</style>

<!-- BODY -->
	<div class="container">
		<div class="main-wrap">
			<div class="row product-detail">
				<div class="col-md-8">
					<p class="font14">Silakan Lengkapi Form dibawah untuk konfirmasi pembayaran anda agar dapat kami proses segera.</p>
					<hr>
					<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label">Nomor Faktur</label>
							<div class="col-sm-9">
								<input type="text" id="invoice" name="invoice" class="form-control" placeholder="Nomor Faktur">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email Anda</label>
							<div class="col-sm-9">
								<input type="email" id="email" name="email" class="form-control" placeholder="Email">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Nama Pemilik Rekening</label>
							<div class="col-sm-9">
								<input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Pemilik Rekening">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Pembayaran Dari Bank</label>
							<div class="col-sm-2 control-label">
							Bank Anda
							</div>
							<div class="col-sm-3">
								<input type="text" name="bankFrom" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-2 col-sm-offset-3 control-label">
								Bank Tujuan
							</div>
							<div class="col-sm-3">
								<select id="namaBank" name="namaBank" class="form-control">
									<option value="">Silakan Pilih</option>
									<option value="">Lippo Bank</option>
									<option value="">Bank BCA</option>
									<option value="">Bank BRI</option>
									<option value="">Bank BNI</option>
									<option value="">Bank BII</option>
									<option value="">Bank Mega</option>
									<option value="">Bank Mandiri</option>
									<option value="">Bank CIMB Niaga</option>
									<option value="">Bank Mandiri Syariah</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Jumlah Dana</label>
							<div class="col-sm-9">
								<div class="input-group">
									<span class="input-group-addon hidden-xs">Rp</span>
										<input id="besarPembayaran" name="besarPembayaran" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
									<span class="input-group-addon hidden-xs">.00</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Tanggal Pembayaran</label>
							<div class="col-sm-9">
								<input class="form-control" id="date" name="payment_date" placeholder="MM/DD/YYY" type="text">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<div><img src="img/captcha.jpg"></div>
								<div class="space10"></div>
								<div style="width: 50%">
									<input class="form-control" required="" type="text" name="captcha" placeholder="Tulis angka verifikasi...">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button class="btn btn-main btn-lg">KONFIRMASI<i class="fa fa-check-square right"></i></button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4">
					<hr class="hidden-md hidden-lg">
					<div class="well">
						<div class="font16">Untuk konfirmasi anda dapat menghubungi fast respon kami di nomor berikut</div>
						<hr>
						<h4>0822 3118 8163</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END -->

<?php include('foot.php'); ?>