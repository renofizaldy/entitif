<?php include("head.php"); ?>

<!-- SLIDER -->
	<div class="container" style="margin-top:15px">
		<div id="front-slide" class="owl-carousel owl-theme" style="width:100%">
			<div class="item">
				<img src='img/converse.jpg'>
			</div>
			<div class="item">
				<img src='img/axs.jpg'>
			</div>
		</div>
	</div>
<!-- END -->

<!-- BODY -->
	<div class="container">
		<div class="main-wrap">
			<div class="promote-product">
				<div class="row">
					<div class="col-md-7 vcenter">
						<img src="img/starter-sets-3-piece-desktop-d161c1c2.png" style="width: 100%">
					</div><!-- 
					 --><div class="col-md-5 text-center vcenter">
						<h3>A STARTER SET IS THE<br>IDEAL WAY TO START</h3>
						<p style="font-size: 16px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
						<div class="price">
							<span class="pr-before">Rp 500,000</span>
							<span class="pr-after">Rp 249,999</span>
						</div>
						<div><a href="cart.php" class="btn btn-lg btn-wow"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Beli Sekarang</a></div>
						<!-- <div><a href="#" class="btn btn-lg btn-whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i>Beli via Whatsapp</a></div> -->
						<div style="margin-top:5px"><small>Berakhir pada 17 Agustus 2017</small></div>
						<!-- <div style='margin-top:5px'><small>Beli via Whatsapp? Klik disini</small></div> -->
					</div>
				</div>
			</div>
            <div class="space30"></div>
			<h4 class="spb_heading spb_tabs_heading"><span>PRODUK TERBARU</span></h4>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="detail.php">
						<div class="pro-img">
							<div class="pro-disc">-25%</div>
							<img src='img/th/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Z.N.E Hoodie</h2>
							<div class="price">
								<span class="before">Rp 2,000,000</span>
								<span class="after">Rp 1,499,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/th/2.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Marvel Avengers Boy</h2>
							<div class="price">
								<span>Rp 50,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<div class="pro-disc">-50%</div>
							<img src='img/3.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Gildan for Kids</h2>
							<div class="price">
								<span class="before">Rp 250,000</span>
								<span class="after">Rp 125,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<div class="pro-disc">-35%</div>
							<img src='img/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title" alt="Adidas Kids Starter Pack">Adidas Kids Starter Pack</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Baju Bekas Anak</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Baju Bekas Anak</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="detail.php">
						<div class="pro-img">
							<div class="pro-disc">-25%</div>
							<img src='img/th/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Z.N.E Hoodie</h2>
							<div class="price">
								<span class="before">Rp 2,000,000</span>
								<span class="after">Rp 1,499,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/th/2.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Marvel Avengers Boy</h2>
							<div class="price">
								<span>Rp 50,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<div class="pro-disc">-50%</div>
							<img src='img/3.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Gildan for Kids</h2>
							<div class="price">
								<span class="before">Rp 250,000</span>
								<span class="after">Rp 125,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<div class="pro-disc">-35%</div>
							<img src='img/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title" alt="Adidas Kids Starter Pack">Adidas Kids Starter Pack</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Baju Bekas Anak</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<div class="product-list"><a href="#">
						<div class="pro-img">
							<img src='img/1.jpg'>
						</div>
						<div class="pro-caption">
							<h2 class="title">Baju Bekas Anak</h2>
							<div class="price">
								<span class="before">Rp 35,000</span>
								<span class="after">Rp 25,000</span>
							</div>
						</div>
					</a></div>
				</div>
			</div>
		</div>
	</div>
<!-- END -->

<?php include('foot.php'); ?>