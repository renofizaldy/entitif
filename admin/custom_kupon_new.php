<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/pages/project.min.css">
<link rel="stylesheet" href="css/separate/pages/editor.min.css">
<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="css/separate/vendor/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="css/separate/vendor/bm-datetimepicker.css">

	<link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
	<link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">

	<style type="text/css">
		.font13 {
			font-size: 13px;
		}
		.drop-zone {
			width: 100%;
		}
		.note-popover.popover {
			display: none;
		}
		.fieldTitle {
			border-bottom: 1px solid #d8e2e7;
			padding-bottom: 10px;
			margin-top: -8px;
		}
		.fieldLine {
			margin-bottom: 10px;
		}
		.fa-margin {
			margin-top: 3px;
			margin-right: 3px;
		}
		#con_min_buy,
		#con_min_use,
		#con_max_time {
			display: none;
		}
	</style>
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Buat Kupon Baru</h3>
						</div>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-lg-8">
					<section class="card">
						<div class="card-block invoice" style="padding-bottom:0">
							<div class="form-group">
								<label class="form-label semibold">Kode Kupon</label>
								<input type="text" class="form-control">
								<small class="text-muted">Pembeli akan menggunakan kode kupon ini saat checkout</small>
							</div>
						</div>
					</section>
					<section class="card">
						<header class="card-header card-header-lg">
							Pilihan
						</header>
						<div class="card-block invoice" style="padding-bottom:5px">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Jenis Kupon</label>
										<select class="manual select2">
											<option>Diskon persen</option>
											<option>Bebas biaya kirim</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Nilai Kupon</label>
										<input type="text" class="form-control" placeholder="Tulis persen (%) dalam angka">
									</div>
								</div>
							</div>
							<div class="checkbox">
								<input type="checkbox" id="check-2">
								<label for="check-2">Kupon memerlukan minimal pembelian</label>
							</div>
							<div class="form-group" id="con_min_buy">
								<label class="form-label semibold">Tulis jumlah minimal pembelian (Rp)</label>
								<input type="text" class="form-control" id="money-mask-input">
							</div>
						</div>
					</section>
					<section class="card">
						<header class="card-header card-header-lg">
							Digunakan untuk
						</header>
						<div class="card-block invoice" style="padding-bottom:5px">
							<div class="radio">
								<input type="radio" name="optionsRadios" id="radio-1" value="option1">
								<label for="radio-1">Semua Produk </label>
							</div>
							<div class="radio">
								<input type="radio" name="optionsRadios" id="radio-2" value="option1">
								<label for="radio-2">Semua Produk Non-Diskon </label>
							</div>
						</div>
					</section>
					<section class="card">
						<header class="card-header card-header-lg">
							Batas Penggunaan
						</header>
						<div class="card-block invoice" style="padding-bottom:5px">
							<div class="checkbox">
								<input type="checkbox" id="check-3">
								<label for="check-3">Batasi jumlah maksimal kupon dapat digunakan.</label>
							</div>
							<div class="form-group" id="con_min_use">
								<label class="form-label semibold">Tulis maksimal penggunaan</label>
								<input id="demo_vertical" type="text" value="" name="demo_vertical">
							</div>
							<div class="checkbox">
								<input type="checkbox" id="check-4">
								<label for="check-4">Batasi untuk satu kali per pengguna.</label>
								<small>(Berdasarkan email yang digunakan)</small>
							</div>
						</div>
					</section>
					<section class="card">
						<header class="card-header card-header-lg">
							Tanggal aktif
						</header>
						<div class="card-block invoice" style="padding-bottom:5px">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Tanggal dimulai</label>
										<div class="form-control-wrapper form-control-icon-right">
											<input type="text" class="form-control datespick-1">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Waktu dimulai</label>
										<div class="form-control-wrapper form-control-icon-right">
											<input type="text" class="form-control timespick-1">
											<i class="fa fa-clock-o"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="checkbox">
								<input type="checkbox" id="check-5">
								<label for="check-5">Atur waktu berakhir</label>
							</div>
							<div class="row" id="con_max_time">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Tanggal berakhir</label>
										<div class="form-control-wrapper form-control-icon-right">
											<input type="text" class="form-control datespick-2">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Waktu berakhir</label>
										<div class="form-control-wrapper form-control-icon-right">
											<input type="text" class="form-control timespick-2">
											<i class="fa fa-clock-o"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="col-lg-4">
					<section class="box-typical proj-page">
						<section class="proj-page-section">
							<header class="proj-page-subtitle padding-sm">
								<h3>Summary</h3>
							</header>
						</section>
						<section class="proj-page-section" style="padding-bottom:10px">
							<a href="#" class="btn btn-lg btn-success" style="width:100%">
								Tambahkan
							</a>
						</section>
					</section>
				</div>
			</div>
		</div>
	</div>

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/select2/select2.full.min.js"></script>
	<script src="js/lib/bootstrap-maxlength/bootstrap-maxlength.js"></script>
	<script src="js/lib/bootstrap-maxlength/bootstrap-maxlength-init.js"></script>
	<script src="js/lib/match-height/jquery.matchHeight.min.js"></script>

	<script src="js/lib/moment/moment.min.js"></script>
	<script src="js/lib/bm-datetime/material.min.js"></script>
	<script src="js/lib/bm-datetime/bm-datetimepicker.js"></script>

	<script src="js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script src="js/lib/input-mask/jquery.mask.min.js"></script>

	<script>
		$(document).ready(function() {
			$('.datespick-1').bootstrapMaterialDatePicker({
				time: false,
				format : 'YYYY-MM-DD',
				minDate: new Date()
			});
			$('.datespick-2').bootstrapMaterialDatePicker({
				time: false,
				format : 'YYYY-MM-DD',
				minDate: new Date()
			});

			$('.timespick-1').bootstrapMaterialDatePicker({
				time: true,
				date: false,
				format : 'HH:mm'
			});
			$('.timespick-2').bootstrapMaterialDatePicker({
				time: true,
				date: false,
				format : 'HH:mm'
			});


			$('#money-mask-input').mask('000.000.000.000.000', {reverse: true});

			$(".select2").select2({
				minimumResultsForSearch: "Infinity"
			});
			$("#check-2").click(function (){
				if ($(this).is(":checked")) {
					$("#con_min_buy").show();
				} else {
					$("#con_min_buy").hide();
				}
			});
			$("#check-3").click(function (){
				if ($(this).is(":checked")) {
					$("#con_min_use").show();
				} else {
					$("#con_min_use").hide();
				}
			});
			$("#check-5").click(function (){
				if ($(this).is(":checked")) {
					$("#con_max_time").show();
				} else {
					$("#con_max_time").hide();
				}
			});
			$("input[name='demo_vertical']").TouchSpin({
				verticalbuttons: true
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>