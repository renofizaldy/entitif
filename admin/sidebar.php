	<style type="text/css">
		ul.side-menu-sub li.with-sub a span.lbl {
			color: #000 !important;
		}
	</style>
	<div class="mobile-menu-left-overlay"></div>
	<nav class="side-menu">
		<ul class="side-menu-list" id="side-menu">
			<li class="brown">
				<a href="custom.php">
					<i class="font-icon font-icon-dashboard"></i>
					<span class="lbl">Dasbor</span>
				</a>
			</li>
			<li class="gold">
				<a href="custom_order.php">
					<i class="fa fa-inbox"></i>
					<span class="lbl">Transaksi</span>
				</a>
			</li>
			<li class="purple with-sub">
				<span>
					<i class="fa fa-cube"></i>
					<span class="lbl">Produk</span>
				</span>
				<ul class="side-menu-sub">
					<li><a href="custom_produk.php"><span class="lbl">Semua Produk</span></a></li>
					<li><a href="custom_draft.php"><span class="lbl">Draft</span></a></li>
					<li><a href="custom_kategori.php"><span class="lbl">Kategori</span></a></li>
					<li><a href="#"><span class="lbl">Flash Sale</span><span class="label label-custom label-pill label-danger">new</span></a></li>
				</ul>
			</li>
			<li class="blue">
				<a href="custom_kupon.php">
					<i class="fa fa-percent"></i>
					<span class="lbl">Kupon</span>
				</a>
			</li>
			<li class="blue">
				<a href="custom_statistik.php">
					<i class="fa fa-pie-chart"></i>
					<span class="lbl">Statistik</span>
				</a>
			</li>
			<li class="blue">
				<a href="custom_halaman.php">
					<i class="fa fa-file-text"></i>
					<span class="lbl">Halaman</span>
				</a>
			</li>
			<li class="green">
				<a href="custom_pesan.php">
					<i class="fa fa-envelope"></i>
					<span class="lbl">Pesan</span>
				</a>
			</li>
			<li class="orange-red with-sub">
				<span>
					<i class="fa fa-gear"></i>
					<span class="lbl">Pengaturan</span>
				</span>
				<ul class="side-menu-sub">
					<li><a href="custom_pengaturan_umum.php"><span class="lbl">Informasi Umum</span></a></li>
					<li><a href="custom_pengaturan_bankkurir.php"><span class="lbl">Bank, Kurir, &amp; Syarat</span></a></li>
					<li><a href="custom_pengaturan_layout.php"><span class="lbl">Logo, Warna, &amp; Layout</span></a></li>
					<li><a href="#"><span class="lbl">Akun</span></a></li>
				</ul>
			</li>
		</ul>
	</nav><!--.side-menu-->