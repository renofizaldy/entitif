<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/pages/invoice.min.css">
<link rel="stylesheet" href="css/separate/pages/widgets.min.css">
<link rel="stylesheet" href="css/separate/pages/profile.min.css">
<link rel="stylesheet" href="css/separate/pages/project.min.css">
<link rel="stylesheet" href="css/separate/pages/others.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">

    <style type="text/css">
    	.invoice .table-details {
    		margin-top: 10px;
    	}
    	.invoice .payment-details {
    		margin-top: 0;
    		width: 100%;
    		float: none;
    	}
    	.friends-list.stripped .friends-list-item {
    		padding-top: 5px;
    		padding-bottom: 5px;
    		background-color: #f0f0f0;
    	}
    	.subtitle {
    		padding-bottom: 10px;
    		font-weight: bold;
    	}
    	.keterangan-small {
    		font-size: 13px;
    		padding-top: 15px;
    		border-top: 1px solid #ccc;
    	}
    	.font13 {
    		font-size: 13px;
    	}
    </style>
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Detail Transaksi</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><span class="label label-default">MENUNGGU</span> - 19:03, Jum 23/11/2029</li>
							</ol>
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block invoice">
					<div class="row">
						<div class="col-lg-5">
							<div class="row">
								<div class="col-xl-5">
									<section class="widget widget-simple-sm">
										<div class="widget-simple-sm-statistic">
										<div class="number">#312312</div>
										<div class="caption color-blue">No. Order</div>
										</div>
									</section>
								</div>
								<div class="col-xl-7">
									<section class="widget widget-simple-sm">
										<div class="widget-simple-sm-statistic">
										<div class="number">Rp 3,500,000</div>
										<div class="caption color-green">Total Transaksi</div>
										</div>
									</section>
								</div>
							</div>
							<table class="table table-sm table-bordered table-hover">
								<tbody>
									<tr>
										<td><b>Sub Total Harga</b></td>
										<td>Rp 123,000</td>
									</tr>
									<tr>
										<td><b>Biaya Pengiriman</b></td>
										<td>Rp 50,000</td>
									</tr>
									<tr>
										<td><b>Total Berat Barang</b></td>
										<td>2 Kg</td>
									</tr>
									<tr>
										<td><b>Pilihan Kurir</b></td>
										<td>Pos Indonesia (Reg)</td>
									</tr>
									<tr>
										<td><b>Kode Kupon</b></td>
										<td>-</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-7">
							<div class="payment-details">
								<strong>Detail Pengiriman</strong>
								<table>
									<tbody>
									<tr>
										<td>Nama Lengkap:</td>
										<td>Yanna Jackson</td>
									</tr>
									<tr>
										<td>Email:</td>
										<td>yanna@scott.com</td>
									</tr>
									<tr>
										<td>Telepon:</td>
										<td>082374324734</td>
									</tr>
									<tr>
										<td>Alamat Lengkap:</td>
										<td>Jl. Letjend. S. Parman, No.64</td>
									</tr>
									<tr>
										<td>Kota &amp; Provinsi:</td>
										<td>Kota Surabaya, Jawa Timur</td>
									</tr>
									<tr>
										<td>Catatan:</td>
										<td>"Tolong dikirim hari ini"</td>
									</tr>
								</tbody></table>
							</div>
						</div>
					</div>
					<br>
					<strong>Daftar Pembelian</strong>
					<div class="row table-details">
						<div class="col-lg-12">
							<table id="table-xs" class="table table-bordered table-hover table-xs">
								<thead>
									<tr>
										<th width="10">#</th>
										<th>Produk</th>
										<th>Catatan</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>#</td>
										<td>Asdsa asdasda</td>
										<td>Description</td>
										<td>Quantity</td>
										<td>Unit Cost</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>#</td>
										<td>Asdsa asdasda</td>
										<td>Description</td>
										<td>Quantity</td>
										<td>Unit Cost</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>#</td>
										<td>Asdsa asdasda</td>
										<td>Description</td>
										<td>Quantity</td>
										<td>Unit Cost</td>
										<td>Total</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
			<div class="row">
				<div class="col-lg-8">
					<section class="card">
						<header class="card-header card-header-lg">
							Aktifitas Konfirmasi Pembayaran <i class="fa fa-question-circle" aria-hidden="true"></i>
						</header>
						<!-- <div class="add-customers-screen tbl">
							<div class="add-customers-screen-in">
								<div class="add-customers-screen-user">
									<i class="fa fa-check"></i>
								</div>
								<h2>Konfirmasi Kosong</h2>
								<p class="lead color-blue-grey-lighter">Belum ada konfirmasi pembayaran terhadap faktur Transaksi ini.</p>
							</div>
						</div> -->
						<div class="card-block invoice">
							<div class="row table-details">
								<div class="col-lg-12">
									<table id="table-xs" class="table table-bordered table-hover table-xs">
										<thead>
											<tr>
												<th>No.Order</th>
												<th>Email</th>
												<th>Waktu Konfirmasi</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><a href="#" data-target="#detailKonfirmasi" data-toggle="modal">#312312</a></td>
												<td>trans@gmail.com</td>
												<td>17:03, Jum - 03/08/2017</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</section>
	            	<section class="box-typical proj-page">
						<section class="proj-page-section proj-page-dates">
							<header class="proj-page-subtitle padding-sm">
								<h3>Info Pembeli</h3>
							</header>
							<div class="tbl">
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">IP Address:</div>
									<div class="tbl-cell">203.142.85.132</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Sistem Operasi:</div>
									<div class="tbl-cell">Android 6.1</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Peramban:</div>
									<div class="tbl-cell">Google Chrome 36</div>
								</div>
							</div>
						</section>
					</section>
				</div>
				<div class="col-lg-4">
	            	<section class="box-typical proj-page">
						<section class="proj-page-section">
							<header class="proj-page-subtitle padding-sm">
								<h3>Status</h3>
							</header>
							<div style="margin-bottom: 7px;">
								<span class="label label-default">MENUNGGU</span>
								<br>
								<small><b>Diperbarui</b>: 19:03, Jum - 23/08/2029</small>
							</div>
							<div><a href="#" data-target="#status" data-toggle="modal" class="btn btn-md btn-success" style="width: 100%"><i class="fa fa-cogs"></i> Ubah Status</a></div>
						</section>
						<section class="proj-page-section">
							<header class="proj-page-subtitle padding-sm">
								<h3>Waktu Transaksi</h3>
							</header>
							19:03, Jumat, 23 November 2029
						</section>
						<section class="proj-page-section">
							<div class="row">
								<div class="col-lg-6">
									<a href="#" class="btn btn-md btn-info" style="width:100%">
										<i class="fa fa-print"></i> Cetak Faktur
									</a>
								</div>
								<div class="col-lg-6">
									<a href="#" class="btn btn-md btn-warning" style="width:100%">
										<i class="fa fa-send"></i> Kirim Faktur
									</a>
								</div>
							</div>
						</section>
					</section>
				</div>
			</div>
	    </div>
	</div>

	<div class="modal fade" tabindex="-1" role="dialog" id="status">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title">Ubah Status Transaksi</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-grey-darker alert-icon alert-close alert-dismissible fade in" role="alert" style="padding-left: 15px;">
						<small><b>Terakhir Diperbarui</b>: 19:03, Jumat - 23 November 2029</small>
					</div>
					<div>
						<div class="subtitle">Pilih Status</div>
						<div class="checkbox-detailed">
							<input type="radio" name="detailed" id="check-det-1" checked/>
							<label for="check-det-1">
							<span class="checkbox-detailed-tbl">
								<span class="checkbox-detailed-cell">
									<span class="checkbox-detailed-title"><i class="font13 fa fa-hourglass-half"></i> Menunggu</span>
									<small>Menunggu pembayaran</small>
								</span>
							</span>
							</label>
						</div>
						<div class="checkbox-detailed">
							<input type="radio" name="detailed" id="check-det-2"/>
							<label for="check-det-2">
							<span class="checkbox-detailed-tbl">
								<span class="checkbox-detailed-cell">
									<span class="checkbox-detailed-title"><i class="font13 fa fa-cogs"></i> Diproses</span>
									<small>Produk sedang diproses</small>
								</span>
							</span>
							</label>
						</div>
						<div class="checkbox-detailed">
							<input type="radio" name="detailed" id="check-det-3"/>
							<label for="check-det-3">
							<span class="checkbox-detailed-tbl">
								<span class="checkbox-detailed-cell">
									<span class="checkbox-detailed-title"><i class="font13 fa fa-truck"></i> Terkirim</span>
									<small>Produk telah dikirim</small>
								</span>
							</span>
							</label>
						</div>
					</div>
					<div style="padding-bottom: 15px;">
						<div class="subtitle">Masukkan Informasi Pengiriman</div>
						<div class="form-group input-group">
							<div class="input-group-btn">
								<button type="button" class="btn btn-default-outline dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilih Kurir</button>
								<div class="dropdown-menu dropdown-menu">
									<a class="dropdown-item" href="#">JNE</a>
									<a class="dropdown-item" href="#">TIKI</a>
									<a class="dropdown-item" href="#">Pos Indonesia</a>
									<div role="separator" class="dropdown-divider"></div>
									<a class="dropdown-item" href="#">Lainnya</a>
								</div>
							</div>
							<input type="text" class="form-control" placeholder="Ketik Nomor Pengiriman ...">
						</div>
						<textarea rows="4" class="form-control" placeholder="Catatan (Opsional) ..."></textarea>
					</div>
					<div class="checkbox">
						<input type="checkbox" id="check-2" checked="">
						<label for="check-2">Kirim email notifikasi ke pembeli</label>
					</div>
					<div class="keterangan-small">
						<b>Keterangan:</b> Status transaksi merupakan informasi yang ditujukan untuk anda sebagai penjual dan pembeli pada halaman "Lacak Pesanan" yang meliputi detail faktur transaksi dan status terbaru transaksi apakah produk sedang di proses hingga telah terkirim ataupun menunggu pembayaran bila pembeli belum melakukan konfirmasi pembayaran.
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Tutup</button>
					<button type="button" class="btn btn-rounded btn-primary" data-dismiss="modal">Simpan</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="detailKonfirmasi">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title" id="myModalLabel">Detail Konfirmasi Pembayaran</h4>
				</div>
				<div class="modal-body">
						<section class="proj-page-section proj-page-dates">
							<!-- <header class="proj-page-subtitle padding-sm">
								<h3>Info Pembeli</h3>
							</header> -->
							<div class="tbl">
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">No. Faktur</div>
									<div class="tbl-cell">#312312</div>
								</div>
								<br>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Email</div>
									<div class="tbl-cell">yanna@scott.com</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Waktu Pembayaran</div>
									<div class="tbl-cell">19:03, Kamis, 22 November 2029</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Nama Pemilik Rekening</div>
									<div class="tbl-cell">Yanna Jackson</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Bank Asal</div>
									<div class="tbl-cell">BCA</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Bank Tujuan</div>
									<div class="tbl-cell">BNI</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Jumlah Dana</div>
									<div class="tbl-cell">Rp 3,500,000</div>
								</div>
								<br>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Dikonfirmasi pada</div>
									<div class="tbl-cell">19:03, Jumat, 23 November 2029</div>
								</div>
							</div>
						</section>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script>
		$(document).ready(function() {
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>