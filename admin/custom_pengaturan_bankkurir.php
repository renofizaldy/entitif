<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="css/separate/pages/project.min.css">
<link rel="stylesheet" href="css/separate/pages/widgets.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Informasi Bank &amp; Kurir</h3>
						</div>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Kurir Pengiriman</strong></div>
					<small>Tentukan kurir pengiriman yang anda gunakan. Informasi ini ditujukan juga pada pembeli saat melakukan pembayaran</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="checkbox-toggle">
								<input type="checkbox" id="check-toggle-1" checked="" />
								<label for="check-toggle-1">JNE</label>
							</div>
							<div class="checkbox-toggle">
								<input type="checkbox" id="check-toggle-2" checked="" />
								<label for="check-toggle-2">TiKi (Titipan Kilat)</label>
							</div>
							<div class="checkbox-toggle" style="margin-bottom:0">
								<input type="checkbox" id="check-toggle-3" checked="" />
								<label for="check-toggle-3">Pos Indonesia</label>
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Rekening Bank</strong></div>
					<small>Tentukan rekening bank yang anda gunakan. Informasi ini ditujukan juga pada pembeli saat melakukan pembayaran</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="form-group">
								<label class="form-label">Bank</label>
								<div class="form-control-wrapper">
									<select class="select2">
										<option>Pilih Bank</option>
										<option>(BNI) Bank Negara Indonesia</option>
										<option>(BRI) Bank Rakyat Indonesia</option>
										<option>(BTN) Bank Tabungan Negara</option>
										<option>(BCA) Bank Central Asia</option>
										<option>Bank Mandiri</option>
										<option>Bank Mandiri Syariah</option>
										<option>Bank Bukopin</option>
										<option>Bank CIMB Niaga</option>
										<option>Bank Danamon Indonesia</option>
										<option>Bank Mega</option>
										<option>Bank Permata</option>
										<option>Bank Panin</option>
										<option>Bank ANZ Indonesia</option>
										<option>Citibank</option>
										<option>HSBC</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Nama Pemilik Rekening</label>
										<input type="text" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Nomor Rekening</label>
										<input type="text" class="form-control" />
									</div>
								</div>
							</div>
							<div class="text-right">
								<a href="#" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle fa-margin"></i> Tambahkan</a>
							</div>
							<hr>
							<table id="table-edit" class="table table-bordered table-hover table-xs">
								<thead>
									<tr>
										<th>Bank</th>
										<th>Nama</th>
										<th>Nomor</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Bank Mandiri</td>
										<td class="color-blue-grey-lighter">Karl Marx</td>
										<td>45347654873</td>
									</tr>
									<tr>
										<td>Bank Central Asia</td>
										<td class="color-blue-grey-lighter">Karl Marx</td>
										<td>45347654873</td>
									</tr>
									<tr>
										<td>Bank Negara Indonesia</td>
										<td class="color-blue-grey-lighter">Karl Marx</td>
										<td>45347654873</td>
									</tr>
									<tr>
										<td>Bank Rakyat Indonesia</td>
										<td class="color-blue-grey-lighter">Karl Marx</td>
										<td>45347654873</td>
									</tr>
								</tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Pernyataan Syarat Layanan</strong></div>
					<small>Bagian ini akan ditampilkan di bawah halaman checkout. Anda bisa membuat dengan contoh template yang tersedia. Contoh template yang tersedia tidak legal secara hukum.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<label class="form-label semibold">Syarat &amp; Ketentuan</label>
							<textarea rows="5" class="form-control"></textarea>
						</div>
						<div class="card-block">
							<label class="form-label semibold">Ketentuan Privasi</label>
							<textarea rows="5" class="form-control"></textarea>
						</div>
						<div class="card-block">
							<label class="form-label semibold">Ketentuan Pengembalian Produk</label>
							<textarea rows="5" class="form-control"></textarea>
						</div>
					</section>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<a href="#" class="btn btn-lg btn-success">Simpan</a>
					<br><br>
				</div>
			</div>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/lib/select2/select2.full.min.js"></script>
	<script src="js/lib/table-edit/jquery.tabledit.min.js"></script>

	<script>
		$(document).ready(function() {
			$(".select2").select2();
			$('#table-edit').Tabledit({
				url: 'example.php',
				columns: {
					identifier: [0, 'id'],
					editable: [[1, 'nama'], [2, 'nomor']]
				}
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>