<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/lib/lobipanel/lobipanel.min.css">
	<link rel="stylesheet" href="css/separate/vendor/lobipanel.min.css">
	<link rel="stylesheet" href="css/lib/jqueryui/jquery-ui.min.css">
	<link rel="stylesheet" href="css/separate/pages/widgets.min.css">

	<link rel="stylesheet" href="css/separate/pages/project.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-xl-8">
	            	<h2>Good Evening, Smith.</h2>
	            	<p class="lead">Here’s what’s happening with your store today.</p>
	            	<p><img src="img/bg/line.png" style="width: 100%;"></p>
	            </div><!--.col-->
	            <div class="col-xl-4">
	            	<section class="box-typical proj-page">
						<section class="proj-page-section proj-page-time-info">
							<div class="tbl">
								<div class="tbl-row">
									<div class="tbl-cell">Time estimated</div>
									<div class="tbl-cell tbl-cell-time">4h 30m</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell">Time working</div>
									<div class="tbl-cell tbl-cell-time">2h 10m</div>
								</div>
							</div>
							<div class="progress-compact-style">
								<progress class="progress progress-success" value="65" max="100">65%</progress>
							</div>
						</section><!--.proj-page-section-->
						<section class="proj-page-section proj-page-labels">
	                        <article class="statistic-box purple">
	                            <div>
	                                <div class="number">3</div>
	                                <div class="caption"><div>Kunjungan Hari Ini</div></div>
	                            </div>
	                        </article>
	                        <article class="statistic-box green" style="margin-bottom: 0">
	                            <div>
	                                <div class="number">Rp 578,000</div>
	                                <div class="caption"><div>Total Penjualan Hari Ini</div></div>
	                            </div>
	                        </article>
						</section><!--.proj-page-section-->

						<section class="proj-page-section proj-page-people">
							<header class="proj-page-subtitle padding-sm">
								<h3>People</h3>
							</header>
							<div class="tbl tbl-people">
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Assignee</div>
									<div class="tbl-cell">
										<div class="user-card-row">
											<div class="tbl-row">
												<div class="tbl-cell tbl-cell-photo">
													<a href="#">
														<img src="img/photo-64-2.jpg" alt="">
													</a>
												</div>
												<div class="tbl-cell">Tim Collins</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Reporter</div>
									<div class="tbl-cell">
										<div class="user-card-row">
											<div class="tbl-row">
												<div class="tbl-cell tbl-cell-photo">
													<a href="#">
														<img src="img/photo-64-2.jpg" alt="">
													</a>
												</div>
												<div class="tbl-cell">Tim Collins</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Votes</div>
									<div class="tbl-cell"><a href="#"><span class="semibold">11</span> vote for this issue</a></div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Watches</div>
									<div class="tbl-cell"><a href="#"><span class="semibold">56</span> start watching this issue</a></div>
								</div>
							</div>
						</section><!--.proj-page-section-->

						<section class="proj-page-section proj-page-dates">
							<header class="proj-page-subtitle padding-sm">
								<h3>Dates</h3>
							</header>
							<div class="tbl">
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Created:</div>
									<div class="tbl-cell">5 days ago</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Updates:</div>
									<div class="tbl-cell">6 days ago</div>
								</div>
							</div>
						</section><!--.proj-page-section-->

						<section class="proj-page-section">
							<header class="proj-page-subtitle padding-sm">
								<h3>Developers</h3>
							</header>
							<a href="#">Create branch</a>
						</section><!--.proj-page-section-->

						<section class="proj-page-section proj-page-agile">
							<header class="proj-page-subtitle padding-sm">
								<h3>Agile</h3>
							</header>
							<div class="tbl">
								<div class="tbl-row">
									<div class="tbl-cell">Future Sprint</div>
									<div class="tbl-cell"><a href="#">Sprit - Signature</a></div>
								</div>
							</div>
							<a href="#">View on Board</a>
						</section><!--.proj-page-section-->

						<section class="proj-page-section">
							<ul class="proj-page-actions-list">
								<li><a href="#"><i class="font-icon font-icon-check-square"></i>Create checklist</a></li>
								<li><a href="#"><i class="font-icon font-icon-alarm-2"></i>Due date</a></li>
								<li><a href="#"><i class="font-icon font-icon-archive"></i>Move to archive</a></li>
							</ul>
						</section><!--.proj-page-section-->
					</section>
	            </div><!--.col-->
	        </div><!--.row-->
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script type="text/javascript" src="js/lib/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/lib/lobipanel/lobipanel.min.js"></script>
	<script type="text/javascript" src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script>
		$(document).ready(function() {
			$('.panel').lobiPanel({
				sortable: true
			});
			$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
				$('.dahsboard-column').matchHeight();
			});
			$(window).resize(function(){
				drawChart();
				setTimeout(function(){
				}, 1000);
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>