<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="css/separate/pages/project.min.css">
<link rel="stylesheet" href="css/separate/pages/gallery.min.css">
<link rel="stylesheet" href="css/lib/summernote/summernote.css"/>
<link rel="stylesheet" href="css/separate/pages/editor.min.css">
<link rel="stylesheet" href="css/lib/jquery-minicolors/jquery.minicolors.css">
<link rel="stylesheet" href="css/separate/vendor/jquery.minicolors.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <style type="text/css">
		.drop-zone {
			width: 100%;
		}
		.note-popover.popover {
			display: none;
		}
    </style>
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Layout Halaman</h3>
						</div>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Logo</strong></div>
					<small>Atur logo yang akan anda gunakan, logo versi wide digunakan pada bagian atas halaman, dan logo square pada bawah halaman. Pastikan ukuran tidak lebih dari 1 MB.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Logo Wide (150px X 30px)</label>
										<input type="file" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Logo Square (150px X 150px)</label>
										<input type="file" class="form-control" />
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Tombol dan Warna</strong></div>
					<small>Atur teks tombol pembelian utama dan warna tombol hingga warna harga dan notifikasi.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="form-group">
								<label class="form-label">Atur Teks Tombol "Beli Sekarang"</label>
								<input type="text" class="form-control" placeholder="Beli Sekarang" />
								<small class="text-muted">Jika kosong akan menggunakan teks "Beli Sekarang"</small>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Warna Tombol "Beli Sekarang"</label>
										<input id="tombolBeliSekarang" type="text" class="form-control" value="#ff5020" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Warna Tombol "Beli via Whatsapp"</label>
										<input id="tombolBeliWhatsapp" type="text" class="form-control" value="#549854" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Warna Harga Diskon</label>
										<input id="warnaHarga" type="text" class="form-control" value="#ff5020" />
										<small class="text-muted">Akan tampil jika anda memberi diskon harga</small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Warna Notifikasi</label>
										<input id="warnaNotif" type="text" class="form-control" value="#ff5020" />
										<small class="text-muted">Informasi diskon dan notifikasi keranjang</small>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Deskripsi Singkat</strong></div>
					<small>Deskripsi akan ditampilkan di bagian atas halaman beranda. Tulis setidaknya minimal 50 karakter. Jika tidak ingin ditampilkan cukup anda kosongkan saja.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="summernote-theme-9">
								<textarea class="summernote" name="name"></textarea>
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Gambar Slide</strong></div>
					<small>Atur slide gambar untuk halaman beranda (Mak. 5 Gambar).</small>
				</div>
				<div class="col-md-8">
					<section class="tabs-section">
						<div class="tabs-section-nav tabs-section-nav-icons">
							<div class="tbl">
								<ul class="nav" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
											<span class="nav-link-in">
												<i class="fa fa-cloud-upload"></i>
												Unggah Gambar
											</span>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
											<span class="nav-link-in">
												<span class="fa fa-image"></span>
												Gambar Slide
											</span>
										</a>
									</li>
								</ul>
							</div>
						</div>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
								<div class="box-typical-upload box-typical-upload-in">
									<div style="margin-bottom: 15px; font-size: 12px;">
										<b>Catatan</b>: Pastikan ukuran gambar anda berbentuk persegi (1170px X 300px, dsb) agar tampilan produk terlihat rapi dan seragam.
									</div>
									<div class="row">
										<div class="col-md-5">
											<div class="drop-zone">
												<i class="font-icon font-icon-cloud-upload-2"></i>
												<div class="drop-zone-caption">Seret berkas foto disini</div>
												<span class="btn btn-rounded btn-file">
												    <span>Pilih gambar</span>
												    <input type="file" name="files[]" multiple="">
												</span>
											</div><!--.drop-zone-->
										</div>
										<div class="col-md-7">
											<h6 class="uploading-list-title">Proses Unggah</h6>
											<ul class="uploading-list">
												<li class="uploading-list-item">
													<div class="uploading-list-item-wrapper">
														<div class="uploading-list-item-name">
															<i class="font-icon font-icon-cam-photo"></i>
															photo.png
														</div>
														<div class="uploading-list-item-size">7,5 mb</div>
														<button type="button" class="uploading-list-item-close">
															<i class="font-icon-close-2"></i>
														</button>
													</div>
													<progress class="progress" value="25" max="100">
														<div class="progress">
															<span class="progress-bar" style="width: 25%;">25%</span>
														</div>
													</progress>
													<div class="uploading-list-item-progress">37% done</div>
													<div class="uploading-list-item-speed">90KB/sec</div>
												</li>
												<li class="uploading-list-item">
													<div class="uploading-list-item-wrapper">
														<div class="uploading-list-item-name">
															<i class="font-icon font-icon-cam-photo"></i>
															dashboard.png
														</div>
														<div class="uploading-list-item-size">7,5 mb</div>
														<button type="button" class="uploading-list-item-close">
															<i class="font-icon-close-2"></i>
														</button>
													</div>
													<progress class="progress" value="100" max="100">
														<div class="progress">
															<span class="progress-bar" style="width: 100%;">100%</span>
														</div>
													</progress>
													<div class="uploading-list-item-progress">Completed</div>
													<div class="uploading-list-item-speed">90KB/sec</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-2">
								<div style="font-size: 12px;">
									<b>Keterangan</b>: Untuk mengatur foto utama produk, arahkan kursor ke foto dan klik pada icon bergambar <i class="fa fa-eye-slash"></i>
								</div>
								<div class="gallery-grid">

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/2.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/1.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/4.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/6.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/7.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Produk di Beranda</strong></div>
					<small>Atur produk yang akan di tampilkan dan bentuk layout di halaman depan.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Produk yang ditampilkan</label>
										<div class="form-control-wrapper">
											<select class="select2">
												<option>Produk Terbaru</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Layout produk</label>
										<div class="form-control-wrapper">
											<select class="select2">
												<option>Thumbnail Produk</option>
												<option>Slide Produk</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<a href="#" class="btn btn-lg btn-success">Simpan</a>
					<br><br>
				</div>
			</div>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/lib/select2/select2.full.min.js"></script>
	<script src="js/lib/summernote/summernote.min.js"></script>
	<script src="js/lib/jquery-minicolors/jquery.minicolors.min.js"></script>
	<script src="js/lib/match-height/jquery.matchHeight.min.js"></script>

	<script>
		$(document).ready(function() {
			$('.gallery-item').matchHeight({
				target: $('.gallery-item .gallery-picture')
			});
			$('.summernote').summernote({
				height: 170,
				placeholder: 'Deskripsi singkat ....'
			});
			$(".select2").select2({
				minimumResultsForSearch: "Infinity"
			});
			$("#tombolBeliSekarang").minicolors({theme: 'bootstrap'});
			$("#tombolBeliWhatsapp").minicolors({theme: 'bootstrap'});
			$("#warnaHarga").minicolors({theme: 'bootstrap'});
			$("#warnaNotif").minicolors({theme: 'bootstrap'});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>