<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/lib/datatables-net/datatables.min.css">
	<link rel="stylesheet" href="css/separate/vendor/datatables-net.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Daftar Transaksi</h3>
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block">
					<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>Invoice</th>
							<th>Waktu</th>
							<th>Nama</th>
							<th>Total</th>
							<th>Status</th>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<th>Invoice</th>
							<th>Waktu</th>
							<th>Nama</th>
							<th>Total</th>
							<th>Status</th>
						</tr>
						</tfoot>
						<tbody>
						<tr>
							<td><a href="custom_order_detail.php">#34361</a></td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-success">TERKIRIM</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-warning">DIPROSES</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						<tr>
							<td>#34361</td>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Rp 320,800</td>
							<td><span class="label label-default">MENUNGGU</span></td>
						</tr>
						</tbody>
					</table>
				</div>
			</section>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/datatables-net/datatables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#example').DataTable({
				"oLanguage": {
					"sSearch"	: "Filter:",
					"sInfo"		: "_START_ - _END_ dari _TOTAL_ Data",
					"sLengthMenu": "_MENU_ Baris",
					"oPaginate" : {
						"sNext"		: ">",
						"sPrevious" : "<"
					}
				}
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>