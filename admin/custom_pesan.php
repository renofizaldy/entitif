<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/pages/project.min.css">
	<link rel="stylesheet" href="css/lib/datatables-net/datatables.min.css">
	<link rel="stylesheet" href="css/separate/vendor/datatables-net.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Pesan Masuk</h3>
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block">
					<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>Waktu</th>
							<th>Nama</th>
							<th>Pesan</th>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<th>Waktu</th>
							<th>Nama</th>
							<th>Pesan</th>
						</tr>
						</tfoot>
						<tbody>
						<tr>
							<td><a href="#" data-target="#detailPesan" data-toggle="modal">2011/04/25</a></td>
							<td>Tiger Nixon</td>
							<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</td>
						</tr>
						<tr>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</td>
						</tr>
						<tr>
							<td>2011/04/25</td>
							<td>Tiger Nixon</td>
							<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</td>
						</tr>
						</tbody>
					</table>
				</div>
			</section>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<div class="modal fade" tabindex="-1" role="dialog" id="detailPesan">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title">Detail Pesan</h4>
				</div>
				<div class="modal-body">
						<section class="proj-page-section proj-page-dates">
							<!-- <header class="proj-page-subtitle padding-sm">
								<h3>Info Pembeli</h3>
							</header> -->
							<div class="tbl">
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Nama</div>
									<div class="tbl-cell">Tiger Nixon</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Email</div>
									<div class="tbl-cell">tigernixon@gmail.com</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Telepon</div>
									<div class="tbl-cell">082231188163</div>
								</div>
								<br>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Pesan</div>
									<div class="tbl-cell">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
									cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
									proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
								</div>
								<br>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Dikirim pada</div>
									<div class="tbl-cell">19:03, Jumat, 23 November 2029</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">IP Address</div>
									<div class="tbl-cell">192.168.1.1</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Peramban</div>
									<div class="tbl-cell">UC Browser Android 5.1</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Sistem Operasi</div>
									<div class="tbl-cell">Android 7.1</div>
								</div>
							</div>
						</section>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-rounded btn-danger">Hapus Pesan</button>
					<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/datatables-net/datatables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#example').DataTable({
				"oLanguage": {
					"sSearch"	: "Filter:",
					"sInfo"		: "_START_ - _END_ dari _TOTAL_ Data",
					"sLengthMenu": "_MENU_ Baris",
					"oPaginate" : {
						"sNext"		: ">",
						"sPrevious" : "<"
					}
				}
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>