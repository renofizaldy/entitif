<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/vendor/bootstrap-daterangepicker.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
	    	<div class="row">
	    		<div class="col-md-7">
					<header class="section-header">
						<div class="tbl">
							<div class="tbl-row">
								<div class="tbl-cell">
									<h3>Statistik Penjualan dan Kunjungan</h3>
									<ol class="breadcrumb breadcrumb-simple">
										<li>25 November 2017</li>
									</ol>
								</div>
							</div>
						</div>
					</header>
	    		</div>
	    		<div class="col-md-5">
					<div class="form-group">
						<label>Pilih rentang waktu</label>
						<div class='input-group date'>
							<input id="daterange" type="text" value="01/01/2015 1:30 PM - 01/01/2015 2:00 PM" class="form-control">
							<span class="input-group-addon">
								<i class="font-icon font-icon-calend"></i>
							</span>
						</div>
					</div>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Total Penjualan
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Total Order
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Total Pengunjung
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Top Landing Pages
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Perangkat digunakan
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Jenis sumber Trafik
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Lokasi kunjungan
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    		<div class="col-md-4">
	    			<section class="card">
						<header class="card-header">
							Sumber Referal
						</header>
						<div class="card-block">
						</div>
					</section>
	    		</div>
	    	</div>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/lib/moment/moment-with-locales.min.js"></script>
	<script src="js/lib/daterangepicker/daterangepicker.js"></script>

	<script>
		$(document).ready(function() {

			$('#daterange').daterangepicker();
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>