<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/pages/project.min.css">
	<link rel="stylesheet" href="css/lib/datatables-net/datatables.min.css">
	<link rel="stylesheet" href="css/separate/vendor/datatables-net.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Kategori Produk</h3>
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="form-label semibold">Tambah Kategori</label>
								<div class="form-control-wrapper form-control-icon-left">
									<input type="text" class="form-control" placeholder="Nama kategori...">
									<i class="fa fa-tag"></i>
								</div>
							</div>
							<button type="button" class="btn btn-md btn-success">Tambahkan</button>
						</div>
						<div class="col-md-8">
							<table id="table-edit" class="table table-sm table-bordered table-hover">
								<thead>
								<tr>
									<th style="display: none">#</th>
									<th>Kategori</th>
									<th class="table-icon-cell">
										<i class="fa fa-cube"></i>
									</th>
								</tr>
								</thead>
								<tbody>
									<tr>
										<td style="display: none">1</td>
										<td class="color-blue-grey-lighter">Semua Produk</td>
										<td class="table-icon-cell">24</td>
									</tr>
									<tr>
										<td style="display: none">1</td>
										<td class="color-blue-grey-lighter">Revene for last quarter in state America for year 2013, whith...</td>
										<td class="table-icon-cell">24</td>
									</tr>
									<tr>
										<td style="display: none">1</td>
										<td class="color-blue-grey-lighter">Revene for last quarter in state America for year 2013, whith...</td>
										<td class="table-icon-cell">24</td>
									</tr>
									<tr>
										<td style="display: none">1</td>
										<td class="color-blue-grey-lighter">Revene for last quarter in state America for year 2013, whith...</td>
										<td class="table-icon-cell">24</td>
									</tr>
								</tbody>
							</table>
							<small><b>Catatan</b>: Menghapus kategori tidak menghapus produk dalam kategori tersebut. Sebagai gantinya, produk dalam kategori yang dihapus akan masuk dalam <b>Semua Produk</b>.</small>
						</div>
					</div>
				</div>
			</section>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/table-edit/jquery.tabledit.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#table-edit').Tabledit({
				url: 'example.php',
				columns: {
					identifier: [0, 'id'],
					editable: [[1, 'kategori']]
				}
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>