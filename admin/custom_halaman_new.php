<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/pages/project.min.css">
<link rel="stylesheet" href="css/lib/summernote/summernote.css"/>
<link rel="stylesheet" href="css/separate/pages/editor.min.css">
<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="css/separate/pages/gallery.min.css">

	<link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
	<link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">

	<style type="text/css">
		.font13 {
			font-size: 13px;
		}
		.drop-zone {
			width: 100%;
		}
		.note-popover.popover {
			display: none;
		}
		.fieldTitle {
			border-bottom: 1px solid #d8e2e7;
			padding-bottom: 10px;
			margin-top: -8px;
		}
		.fieldLine {
			margin-bottom: 10px;
		}
		.fa-margin {
			margin-top: 3px;
			margin-right: 3px;
		}
	</style>
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Tambah Halaman Baru</h3>
						</div>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-lg-8">
					<section class="card">
						<div class="card-block invoice">
							<div class="form-group">
								<label class="form-label semibold">Judul Halaman</label>
								<div class="form-control-wrapper form-control-icon-left">
									<input type="text" class="form-control" placeholder="">
									<i class="fa fa-file-text"></i>
								</div>
							</div>
							<div class="summernote-theme-9">
								<textarea class="summernote" name="name"></textarea>
							</div>
						</div>
					</section>
					<section class="card">
						<header class="card-header card-header-lg">
							Custom SEO
						</header>
						<div class="card-block invoice">
							<div class="form-group">
								<label class="form-label semibold">Judul</label>
								<input type="text" class="form-control maxlength-simple" maxlength="70">
								<small class="text-muted">Mak. 70 karakter</small>
							</div>
							<div class="form-group">
								<label class="form-label semibold">Deskripsi</label>
								<textarea rows="4" class="form-control maxlength-simple" maxlength="160"></textarea>
								<small class="text-muted">Mak. 160 karakter</small>
							</div>
							<div class="form-group">
								<label class="form-label semibold">URL</label>
								<div class="form-control-wrapper">
									<input type="text" class="form-control" placeholder="">
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="col-lg-4">
					<section class="box-typical proj-page">
						<section class="proj-page-section">
							<header class="proj-page-subtitle padding-sm">
								<h3>Terakhir disimpan</h3>
							</header>
							19:03, Jumat, 23 November 2029
						</section>
						<section class="proj-page-section" style="padding-bottom:10px">
							<a href="#" class="btn btn-lg btn-success" style="width:100%">
								Tambahkan
							</a>
						</section>
					</section>
				</div>
			</div>
		</div>
	</div>

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/bootstrap-maxlength/bootstrap-maxlength.js"></script>
	<script src="js/lib/bootstrap-maxlength/bootstrap-maxlength-init.js"></script>
	<script src="js/lib/summernote/summernote.min.js"></script>
	<script type="text/javascript" src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	<script>
		$(document).ready(function() {
			$('.summernote').summernote({
				height: 170,
				placeholder: 'Konten halaman ....'
			});
		});
	</script>

<script src="js/app.js"></script>
</body>
</html>