<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/pages/project.min.css">
<link rel="stylesheet" href="css/lib/summernote/summernote.css"/>
<link rel="stylesheet" href="css/separate/pages/editor.min.css">
<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="css/separate/pages/gallery.min.css">

	<link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
	<link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">

	<style type="text/css">
		.font13 {
			font-size: 13px;
		}
		.drop-zone {
			width: 100%;
		}
		.note-popover.popover {
			display: none;
		}
		.fieldTitle {
			border-bottom: 1px solid #d8e2e7;
			padding-bottom: 10px;
			margin-top: -8px;
		}
		.fieldLine {
			margin-bottom: 10px;
		}
		.fa-margin {
			margin-top: 3px;
			margin-right: 3px;
		}
	</style>
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Tambah Produk Baru</h3>
						</div>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-lg-8">
					<section class="card">
						<div class="card-block invoice">
							<div class="form-group">
								<label class="form-label semibold">Nama Produk</label>
								<div class="form-control-wrapper form-control-icon-left">
									<input type="text" class="form-control" placeholder="">
									<i class="fa fa-cube"></i>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Kategori</label>
										<div class="form-control-wrapper">
											<select class="select2">
												<option>Baju Anak</option>
												<option>Baju Pria</option>
												<option>Baju Wanita</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Berat (gram)</label>
										<div class="form-control-wrapper">
											<input type="text" class="form-control" placeholder="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Harga</label>
										<div class="form-control-wrapper">
											<input id="money-mask-input" type="text" class="form-control" placeholder="">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label semibold">Diskon Harga (%) *Opsional</label>
										<div class="form-control-wrapper">
											<input type="text" class="form-control" placeholder="">
										</div>
									</div>
								</div>
							</div>
							<div class="summernote-theme-9">
								<textarea class="summernote" name="name"></textarea>
							</div>
						</div>
					</section>

					<section class="tabs-section">
						<div class="tabs-section-nav tabs-section-nav-icons">
							<div class="tbl">
								<ul class="nav" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
											<span class="nav-link-in">
												<i class="fa fa-cloud-upload"></i>
												Unggah Foto
											</span>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
											<span class="nav-link-in">
												<span class="fa fa-image"></span>
												Gambar Foto
											</span>
										</a>
									</li>
								</ul>
							</div>
						</div>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
								<div class="box-typical-upload box-typical-upload-in">
									<div style="margin-bottom: 15px; font-size: 12px;">
										<b>Catatan</b>: Pastikan ukuran gambar anda berbentuk persegi (500px X 500px, dsb) agar tampilan produk terlihat rapi dan seragam.
									</div>
									<div class="row">
										<div class="col-md-5">
											<div class="drop-zone">
												<i class="font-icon font-icon-cloud-upload-2"></i>
												<div class="drop-zone-caption">Seret berkas foto disini</div>
												<span class="btn btn-rounded btn-file">
												    <span>Pilih gambar</span>
												    <input type="file" name="files[]" multiple="">
												</span>
											</div><!--.drop-zone-->
										</div>
										<div class="col-md-7">
											<h6 class="uploading-list-title">Proses Unggah</h6>
											<ul class="uploading-list">
												<li class="uploading-list-item">
													<div class="uploading-list-item-wrapper">
														<div class="uploading-list-item-name">
															<i class="font-icon font-icon-cam-photo"></i>
															photo.png
														</div>
														<div class="uploading-list-item-size">7,5 mb</div>
														<button type="button" class="uploading-list-item-close">
															<i class="font-icon-close-2"></i>
														</button>
													</div>
													<progress class="progress" value="25" max="100">
														<div class="progress">
															<span class="progress-bar" style="width: 25%;">25%</span>
														</div>
													</progress>
													<div class="uploading-list-item-progress">37% done</div>
													<div class="uploading-list-item-speed">90KB/sec</div>
												</li>
												<li class="uploading-list-item">
													<div class="uploading-list-item-wrapper">
														<div class="uploading-list-item-name">
															<i class="font-icon font-icon-cam-photo"></i>
															dashboard.png
														</div>
														<div class="uploading-list-item-size">7,5 mb</div>
														<button type="button" class="uploading-list-item-close">
															<i class="font-icon-close-2"></i>
														</button>
													</div>
													<progress class="progress" value="100" max="100">
														<div class="progress">
															<span class="progress-bar" style="width: 100%;">100%</span>
														</div>
													</progress>
													<div class="uploading-list-item-progress">Completed</div>
													<div class="uploading-list-item-speed">90KB/sec</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-2">
								<div style="font-size: 12px;">
									<b>Keterangan</b>: Untuk mengatur foto utama produk, arahkan kursor ke foto dan klik pada icon bergambar <i class="fa fa-eye-slash"></i>
								</div>
								<div class="gallery-grid">

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/2.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/1.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/4.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/6.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->

									<div class="gallery-col">
										<article class="gallery-item">
											<img class="gallery-picture" src="img/produk/7.jpg" alt="" height="140">
											<div class="gallery-hover-layout">
												<div class="gallery-hover-layout-in">
													<div class="btn-group">
														<button type="button" class="btn">
															<i class="fa fa-eye-slash"></i>
														</button>
														<button type="button" class="btn">
															<i class="fa fa-trash"></i>
														</button>
													</div>
													<p>3 days ago</p>
												</div>
											</div>
										</article>
									</div><!--.gallery-col-->
								</div>
							</div>
						</div>
					</section>
				</div>


				<div class="col-lg-4">
					<section class="box-typical proj-page">
						<section class="proj-page-section proj-page-dates" style="padding-bottom:5px">
							<header class="proj-page-subtitle">
								<h3 class="with-border">Inventori <i class="fa fa-question-circle"></i></h3>
							</header>
							<div class="form-group">
								<label class="form-label semibold">SKU (Stock Keeping Unit)</label>
								<div class="form-control-wrapper">
									<input type="text" class="form-control" placeholder="">
								</div>
							</div>
						</section>
					</section>
					<section class="box-typical proj-page">
						<section class="proj-page-section proj-page-dates" style="padding-bottom:10px">
							<header class="proj-page-subtitle">
								<div class="row fieldTitle">
									<div class="col-md-6">
										<h3 style="margin-top:5px;">Varian <i class="fa fa-question-circle"></i></h3>
									</div>
									<div class="col-md-6 text-right">
										<small><a href="#" onclick="tambahField();" class="btn btn-sm btn-rounded btn-primary"><i class="fa fa-plus-circle fa-margin"></i> Tambah Varian</a></small>
									</div>
								</div>
							</header>
							<div class="fieldList">
								<div class="row fieldLine">
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Cth: Ukuran ..">
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Cth: S/M/L ..">
									</div>
								</div>
							</div>
						</section>
					</section>

					<!-- <section class="box-typical proj-page">
						<section class="proj-page-section proj-page-dates">
							<header class="proj-page-subtitle padding-sm">
								<h3 class="with-border">Custom SEO</h3>
							</header>
							<div class="tbl">
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">IP Address:</div>
									<div class="tbl-cell">203.142.85.132</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Sistem Operasi:</div>
									<div class="tbl-cell">Android 6.1</div>
								</div>
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-lbl">Peramban:</div>
									<div class="tbl-cell">Google Chrome 36</div>
								</div>
							</div>
						</section>
					</section> -->

					<section class="box-typical proj-page">
						<section class="proj-page-section">
							<header class="proj-page-subtitle padding-sm">
								<h3>Tombol Pembelian <i class="fa fa-question-circle"></i></h3>
							</header>
							Klik salah satu jika ingin menon-aktifkan
						</section>
						<section class="proj-page-section" style="padding-bottom:0">
							<div class="checkbox-bird orange">
								<input type="checkbox" id="check-bird-13" checked="">
								<label for="check-bird-13">Beli Sekarang (Tambah ke keranjang)</label>
							</div>
							<div class="checkbox-bird green">
								<input type="checkbox" id="check-bird-11" checked="">
								<label for="check-bird-11">Beli via WhatsApp</label>
							</div>
						</section>
					</section>

					<section class="box-typical proj-page">
						<section class="proj-page-section">
							<header class="proj-page-subtitle padding-sm">
								<h3>Terakhir disimpan</h3>
							</header>
							19:03, Jumat, 23 November 2029
						</section>
						<section class="proj-page-section" style="padding-bottom:10px">
							<div class="row">
								<div class="col-lg-6">
									<a href="#" class="btn btn-inline btn-primary-outline" style="width:100%">
										Simpan Draft
									</a>
								</div>
								<div class="col-lg-6">
									<a href="#" class="btn btn-md btn-success" style="width:100%">
										Tambahkan
									</a>
								</div>
							</div>
						</section>
					</section>
				</div>
			</div>
		</div>
	</div>

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/select2/select2.full.min.js"></script>
	<script src="js/lib/input-mask/jquery.mask.min.js"></script>
	<script src="js/lib/summernote/summernote.min.js"></script>
	<script src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	<script>
		$(document).ready(function() {
			$('.gallery-item').matchHeight({
				target: $('.gallery-item .gallery-picture')
			});
			$('.summernote').summernote({
				height: 170,
				placeholder: 'Deskripsi produk ....'
			});
			$(".select2").select2();
			$('#money-mask-input').mask('000.000.000.000.000', {reverse: true});
		});
		function tambahField() {
			var field = "<div class='row fieldLine'><div class='col-md-6'><input type='text' class='form-control' placeholder='Cth: Ukuran ..'></div><div class='col-md-6'><input type='text' class='form-control' placeholder='Cth: S/M/L ..'></div></div>";
			jQuery(".fieldList").append(field);
		}
	</script>

<script src="js/app.js"></script>
</body>
</html>