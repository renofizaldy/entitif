<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Entitif Admin</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="css/separate/pages/project.min.css">
<link rel="stylesheet" href="css/separate/pages/widgets.min.css">

    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu dark-theme dark-theme-blue">

	<?php require 'header.php'; ?>

	<?php require 'sidebar.php'; ?>

	<div class="page-content">
	    <div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Informasi Umum</h3>
						</div>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Detail Toko</strong></div>
					<small>Entitif dan pembeli akan menggunakan informasi ini untuk menghubungi anda.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="form-group">
								<label class="form-label">Nama Toko</label>
								<input type="text" class="form-control" />
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Email untuk Akun</label>
										<input type="text" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Email untuk Pembeli</label>
										<input type="text" class="form-control" />
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Alamat Toko</strong></div>
					<small>Alamat akan digunakan untuk tampilan informasi pada pengguna dan untuk mengkalkulasi biaya pengiriman.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="form-group">
								<label class="form-label">Nomor Telepon</label>
								<input type="text" class="form-control" />
							</div>
							<div class="form-group">
								<label class="form-label">Alamat</label>
								<input type="text" class="form-control" />
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Kota</label>
										<input type="text" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Kode Pos</label>
										<input type="text" class="form-control" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="form-label">Provinsi</label>
								<input type="text" class="form-control" />
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Tentang Toko</strong></div>
					<small>Deskripsi akan di tampilkan pada bagian bawah halaman, anda dapat mengisi dengan informasi singkat tentang seputar toko anda.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<textarea rows="4" class="form-control"></textarea>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Akun Sosial</strong></div>
					<small>Akun sosial digunakan untuk menampilkan akun media sosial anda pada bagian footer halaman dan pada bagian detail produk, khusus pada bagian <b>Nomor WhatsApp</b> akan dapat digunakan untuk fitur pembelian melalui WhatsApp.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="form-group">
								<label class="form-label semibold">Nomor WhatsApp</label>
								<div class="form-control-wrapper form-control-icon-left">
									<input type="text" class="form-control" placeholder="">
									<i class="fa fa-whatsapp"></i>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Facebook</label>
										<input type="text" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Instagram</label>
										<input type="text" class="form-control" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">YouTube</label>
										<input type="text" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Twitter</label>
										<input type="text" class="form-control" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">LINE</label>
										<input type="text" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">BBM</label>
										<input type="text" class="form-control" />
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div><strong>Standar dan Format</strong></div>
					<small>Standar dan Format digunakan menentukan zona waktu wilayah dan mata uang.</small>
				</div>
				<div class="col-md-8">
					<section class="card">
						<div class="card-block">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Zona Waktu</label>
										<div class="form-control-wrapper">
											<select class="select2">
												<option>(WIB) Waktu Indonesia bagian Barat</option>
												<option>(WITA) Waktu Indonesia bagian Tengah</option>
												<option>(WIT) Waktu Indonesia bagian Timur</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Mata Uang</label>
										<div class="form-control-wrapper">
											<select class="select2">
												<option>(Rp) Rupiah</option>
												<option>($) US Dolar</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<a href="#" class="btn btn-lg btn-success">Simpan</a>
					<br><br>
				</div>
			</div>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/lib/select2/select2.full.min.js"></script>

	<script>
		$(document).ready(function() {
			$(".select2").select2({
				minimumResultsForSearch: "Infinity"
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>
</html>